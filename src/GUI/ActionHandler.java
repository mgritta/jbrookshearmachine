package GUI;

import InputOutput.Converter;
import InputOutput.FileIO;
import InputOutput.Interpreter;
import MainEngine.Executor;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 * An action handler class to offload a lot of the work
 * done by class MainWindow reducing code by about 60%. This object handles all 
 * implementation of any propagated tasks from the MainWindow. The reasoning here is that 
 * to keep the MainWindow class as short as possible by propagating work to this object.
 * 
 * @author Milan Gritta
 */
public class ActionHandler {

    private FileIO fileIO;
    private Executor executor;
    private Thread thread;
    private JTable memory, cpu;

    /**
     * Constructor for ActionHandler objects. Creates an instance of FileIO class
     * used to handle input/output.
     * @param memory 
     * @param cpu 
     */
    protected ActionHandler(JTable memory, JTable cpu) {
        fileIO = new FileIO();
        executor = new Executor(memory, cpu);
        this.memory = memory;
        this.cpu = cpu;
    }

    /**
     * Resets main memory table to default values. This method populates the memory JTable with default values
     * erasing any previous data.
     * 
     */
    protected void resetMainMemory() {
        Object data[][] = {{Converter.decToHex(255), "00000000", "00", null, "0", "0.000", null}};
        String[] columns = new String[]{"Address", "Binary", "Hex", "ASCII", "Integer", "Float", "Instruction"};
        DefaultTableModel model = new DefaultTableModel(data, columns) {
        boolean[] canEdit = new boolean [] {false, true, true, true, true, true, false};
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }};
        for (int i = 0; i < 255; i++) {
            model.insertRow(i, new Object[]{Converter.decToHex(i), "00000000", "00", null, "0", "0.000", null});
        }
        memory.setModel(model);
        setCellAlignment(memory);
        MainWindow.printText("");
        setColumnWidth();
        executor.resetStack();
    }

    /**
     * Resets the registers table to default values. This method populates the CPU JTable with default values
     * erasing any previous data.
     * 
     */
    protected void resetRegisters() {
        Object data[][] = {{Converter.decToHex(15).charAt(1), "00000000", "00", null, "0", "0.000"}};
        String[] columns = new String[]{"Register", "Binary", "Hex", "ASCII", "Integer", "Float"};
        DefaultTableModel model = new DefaultTableModel(data, columns){
        boolean[] canEdit = new boolean [] {false, false, false, false, false, false};
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }};  
        for (int i = 0; i < 15; i++) {
            model.insertRow(i, new Object[]{Converter.decToHex(i).charAt(1), "00000000", "00", null, "0", "0.000"});
        }
        cpu.setModel(model);
        setCellAlignment(cpu);
        memory.setRowSelectionInterval(0, 0);
        memory.setColumnSelectionInterval(0, 6);
        MainWindow.setCounter("00");
        MainWindow.printText("");
        executor.resetClockCycles();
        setColumnWidth();
        executor.resetStack();
    }
    
    private void setCellAlignment(JTable table) {
        table.getTableHeader().setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();    
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);  
        for (int i = 0; i < 6; i++) {
            TableColumn colCpu = table.getColumnModel().getColumn(i);
            colCpu.setCellRenderer(dtcr);
        }
    }

    /**
     * Sets column ramWidth for main memory. The column ramWidth for the memory table 
     * needs to be adjusted and any content in both tables needs to be centred.
     * 
     */
    protected void setColumnWidth() {
        int[] ramWidth = {40, 83, 38, 35, 53, 50, 205}; 
        int[] cpuWidth = {65,115,55,45,85,85};
        for (int i = 0; i < 7; i++) {
            TableColumn colMem = memory.getColumnModel().getColumn(i);
            colMem.setPreferredWidth(ramWidth[i]); 
        }
        for (int i = 0; i < 6; i++) {
            TableColumn colMem = cpu.getColumnModel().getColumn(i);
            colMem.setPreferredWidth(cpuWidth[i]);   
        }
        setCellAlignment(memory);
        setCellAlignment(cpu);
        memory.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        cpu.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    }

    /**
     * Handles property change for main memory. When the user finished editing a cell,
     * an event is fired which is handles here. The relevant row will be updated with the new
     * values.
     * 
     * @param evt the property change event informing of the type of event.
     */
    protected void memoryPropertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equalsIgnoreCase("ancestor") || evt.getPropertyName().equalsIgnoreCase("model")) {
            return;
        }
        if (evt.getNewValue() == null && memory.getSelectedRow() != -1) {
            updateMemoryRow(memory.getSelectedRow(), memory.getSelectedColumn());
        }
    }

    private void updateMemoryRow(int row, int col) {
        MainWindow.printText("");
        row = (row == -1) ? 0 : row;
        col = (col == -1) ? 0 : col;
        Object cellContents = memory.getValueAt(row, col);
        Object[] array = Converter.convert(cellContents, col - 1);
        if (array == null) {
            MainWindow.printText("Invalid input on line " + Converter.decToHex(row) + " column " + (col + 1)
                    + "\n\nPossible causes:\n-Number base in wrong column\n-Illegal characters\n-Input outside range");
            return;
        }
        String currentCell = (row % 2 == 0) ? array[1].toString() : memory.getValueAt(row - 1, 2).toString();
        String next = (row % 2 == 0) ? memory.getValueAt(row + 1, 2).toString() : array[1].toString();
        next = (next.equals("null")) ? "00" : next;
        String instruction = Interpreter.getInstruction(currentCell.charAt(0), "" + currentCell.substring(1, 2)
                + next.substring(0, 1) + next.substring(1, 2), MainWindow.isAssembler());
        memory.setValueAt(array[4], row, 5);
        memory.setValueAt(array[3], row, 4);
        memory.setValueAt(array[2], row, 3);
        memory.setValueAt(array[1], row, 2);
        memory.setValueAt(array[0], row, 1);
        MainWindow.updateBitmap();
        row = (row % 2 == 0) ? row : row - 1;
        memory.setValueAt(instruction, row, 6);
    }

    /**
     * Switches from descriptive mode to assembler mode an vice versa. When the user requests
     * a change in the format of the instructions via the radio buttons, this method performs
     * the change.
     */
    protected void switchMode() {
        for (int i = 0; i < 255; i++) {
            if (i % 2 == 0) {
                String even = memory.getValueAt(i, 2).toString();
                String odd = memory.getValueAt(i + 1, 2).toString();
                if (even.equals("00")) {
                    continue;
                }
                String instruction = Interpreter.getInstruction(even.charAt(0), "" + even.substring(1, 2)
                        + odd.substring(0, 1) + odd.substring(1, 2), MainWindow.isAssembler());
                memory.setValueAt(instruction, i, 6);
            }
        }
    }

    /**
     * Assembles and loads code. Opens a file chooser dialogue for the user to select
     * the text file, the code is converted and loaded into the memory table.
     * @param win the instance of the main window used to position the file chooser dialogue
     */
    protected void assebleLoadButtonPressed(Component win) {
        File file = getFile(win);
        if (file == null) {
            return;
        }
        ArrayList<String> list = fileIO.assembleCode(file);
        if (list == null) {
            return;
        }
        int row, length;
        resetMainMemory();
        for (String line : list) {
            row = Integer.parseInt(line.substring(0, 2), 16);
            line = line.substring(3, line.indexOf("//")).trim();
            if(line.length() % 2 != 0) {
                MainWindow.printText("Invalid instruction on this line:\n" + line);
                return;
            }
            length = 0;
            while(length < line.length()) {
                if(!memory.getValueAt(row, 2).toString().equals("00")) {
                    MainWindow.printText("Cannot assemble to cell " + Converter.decToHex(row)
                    + ". It is used by the DATA instruction as a label!");
                    return;
                }
                memory.setValueAt(line.substring(length, length + 2), row, 2);
                updateMemoryRow(row - 1, 2);
                updateMemoryRow(row, 2);
                row++;
                length += 2;
            }           
        }
    }

    /**
     * Loads assembled program from file. Opens a file chooser dialogue for the user to select
     * the text file and the machine code is loaded into the memory table.
     * @param win the instance of the main window used to position the file chooser
     */
    protected void loadFromFileButtonPressed(Component win) {
        File file = getFile(win);
        if (file == null) {
            return;
        }
        ArrayList<String> list = fileIO.readMachineCode(file);
        if (list == null) {
            return;
        }
        int row = 0, length;
        resetMainMemory();
        for (String line : list) {
            if(line.length() % 2 != 0) {
                MainWindow.printText("Invalid instruction on this line:\n" + line);
                return;
            }
            if(line.startsWith("--")) {
                if(line.length() < 4) {
                    MainWindow.printText("Illegal label error!\n\nHas to be \"FF:\" for example..");
                    return;
                }
                row = Integer.parseInt(line.substring(2, 4),16);
                line = line.substring(4).trim();
            }
            length = 0;
            while(length < line.length()) {
                if(!memory.getValueAt(row, 2).toString().equals("00")) {
                    MainWindow.printText("Cannot load to cell " + Converter.decToHex(row)
                    + ". It is used by an instruction as a label!");
                    return;
                }
                memory.setValueAt(line.substring(length, length + 2), row, 2);
                updateMemoryRow(row - 1, 2);
                updateMemoryRow(row, 2);
                row++;
                length += 2;
            }
        }
        setColumnWidth();
    }

    /**
     * Saves the machine code instructions to text file. Opens a file chooser dialogue
     * for the user to select the destination folder to save the contents of the memory
     * table. A warning is given if a file already exists.
     * @param win the instance of main window used to position the file chooser
     */
    protected void saveInFileButtonPressed(Component win) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Save machine code:");
        fc.addChoosableFileFilter(new TextFileFilter());
        fc.setMultiSelectionEnabled(false);
        File file;
        int returnValue = 0;
        do {
            returnValue = fc.showSaveDialog(win);
            file = fc.getSelectedFile();
            if(file == null) {
                return;
            }
            if (returnValue == JFileChooser.CANCEL_OPTION) {
                return;
            }
            if (file.exists()) {
                int response = JOptionPane.showConfirmDialog(win, "This file already exists. Overwrite?", "Overwrite?", JOptionPane.YES_NO_OPTION);
                if (response == JOptionPane.OK_OPTION) {
                    break;
                }
            }
        } while (file.exists());
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            String path = (file.exists()) ? fc.getSelectedFile().getAbsolutePath() : fc.getSelectedFile().getAbsolutePath() + ".txt";
            ArrayList<String> list = new ArrayList<>();
            int row = 0;
            while (row < 256) {
                String code = memory.getValueAt(row, 2).toString();
                if (code.equals("00")) {
                    row += 2;
                    continue;
                }
                String line = memory.getValueAt(row, 0).toString() + ": " + code + memory.getValueAt(row + 1, 2).toString()
                        + " // " + memory.getValueAt(row, 6).toString();
                list.add(line);
                row += 2;
            }
            fileIO.saveCode(list, path);
        }
    }

    private File getFile(Component win) {
        JFileChooser fc = new JFileChooser();
        fc.addChoosableFileFilter(new TextFileFilter());
        fc.setMultiSelectionEnabled(false);
        if(FileIO.currentDirectory != null) {
            fc.setCurrentDirectory(new File(FileIO.currentDirectory));
        }
        int returnValue = fc.showDialog(win, "Load My Code");
        File file = fc.getSelectedFile();
        if (file == null) {
            return null;
        }
        String filename = file.toString();
        FileIO.currentDirectory = filename;
        if (!filename.endsWith("txt")) {
            MainWindow.printText("Only text files are supported!");
            return null;
        }
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return file;
        }
        return null;
    }

    /**
     * Assembles code to file. Opens the file chooser dialogue for the user to choose the
     * text file containing assembly code, converts it to machine code and saves to the
     * desired folder.
     * @param win the instance of main window used to position the window
     */
    protected void assembleFileButtonPressed(Component win) {
        File file = getFile(win);
        if (file == null) {
            return;
        }
        ArrayList<String> list = fileIO.assembleCode(file);
        if (list == null) {
            return;
        }
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Assemble to file:");
        fc.addChoosableFileFilter(new TextFileFilter());
        int returnValue = 0;
        boolean exists = true;
        do {
            returnValue = fc.showSaveDialog(win);
            exists = fc.getSelectedFile().exists();
            if (returnValue == JFileChooser.CANCEL_OPTION) {
                return;
            }
            if (exists) {
                int response = JOptionPane.showConfirmDialog(win, "This file already exists. Overwrite?", "Overwrite?", JOptionPane.YES_NO_OPTION);
                if (response == JOptionPane.OK_OPTION) {
                    break;
                }
            }
        } while (exists);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            String path = (exists) ? fc.getSelectedFile().getAbsolutePath() : fc.getSelectedFile().getAbsolutePath() + ".txt";
            fileIO.saveCode(list, path);
        }
        setColumnWidth();
    }

    /**
     * Performs one step. When the user presses the Step button, this method uses the
     * executor object to execute one instruction only.
     */
    protected void stepButtonPressed() {
        if (thread != null && thread.isAlive()) {
            return;
        }
        try {
            MainWindow.printText("");
            executor.execute();
        } catch (Exception e) {
            MainWindow.printText("Invalid Instruction!\n\n" + e.getMessage());
        }
    }

    /**
     * Resumes execution. If and only if the execution has been paused, this method
     * resumes the execution at the last known  program counter value.
     */
    protected void continueButtonPressed() {
        if (thread != null) {
            if (thread.isAlive()) {
                thread.interrupt();
                MainWindow.setButtonText("Continue");
            } 
            else {
                thread = new Thread(new Executor(memory, cpu));
                thread.start();
                MainWindow.setButtonText("Stop");
                MainWindow.printText("");
            }
        }
    }

    /**
     * Executes continuously. This method starts a new thread if and only if a current 
     * thread isn't already executing. If it is, it stops that execution and starts at
     * the beginning.
     */
    protected void resetRunbuttonPressed() {
        if (thread != null) {
            thread.interrupt();
        }
        resetRegisters();
        thread = new Thread(new Executor(memory, cpu));
        thread.start();
        MainWindow.setButtonText("Stop");
        executor.resetStack();
        MainWindow.printText("");
        setColumnWidth();
    }
    
    /**
     * Undo one step button pressed, please handle.
     */
    protected void backButtonPressed() {
        if (thread != null && thread.isAlive()) {
            return;
        }
        executor.doStepBack();
    }
    
    /**
     * Saves the bitmap display as image to the desired location. Opens a file
     * chooser dialogue for the user to select the destination folder for the PNG 
     * image to be saved.
     * @param panel the bitmap display object to convert to .png
     * @param win the instance of main window to position the file chooser
     */
    protected void saveBitmapAsImage(JPanel panel, Component win) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Save bitmap image:");
        fc.setMultiSelectionEnabled(false);
        int returnValue;
        File file;
        do {
            returnValue = fc.showSaveDialog(win);
            file = fc.getSelectedFile();
            if(file == null) {
                return;
            }
            if (returnValue == JFileChooser.CANCEL_OPTION) {
                return;
            }
            if (file.exists()) {
                int response = JOptionPane.showConfirmDialog(win, "This file already exists. Overwrite?", "Overwrite?", JOptionPane.YES_NO_OPTION);
                if (response == JOptionPane.OK_OPTION) {
                    break;
                }
            }
        } while (file.exists());
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            int [] size = MainWindow.getBitmapSize();
            BufferedImage bi = new BufferedImage(size[0], size[1], BufferedImage.TYPE_INT_ARGB); 
            Graphics2D g = bi.createGraphics();
            panel.paint(g);
            g.dispose();
            try {
                String path = (file.exists()) ? fc.getSelectedFile().getAbsolutePath() : fc.getSelectedFile().getAbsolutePath() + ".png";
                ImageIO.write(bi, "png", new File(path));
            } catch (IOException ex) {
                MainWindow.printText("Unable to save your image. Why?\n" + ex.getMessage());
            }
        }
    }

    private static class TextFileFilter extends FileFilter {
        @Override
        public boolean accept(File file) {
            String filename = file.getName();
            return filename.toLowerCase(new Locale("en", "GB")).endsWith(".txt") || file.isDirectory();
        }
        @Override
        public String getDescription() {
            return "Text Documents (.txt)";
        }
    }
    
    /**
     * Shuts down the application with care. Asks user if they want to abort current computation if one is running.
     */
    protected void shutDown() {
        if(thread != null && thread.isAlive()) {
            int n = JOptionPane.showConfirmDialog(null, "The machine is still running... You will lose the current computation... Quit anyway?",
                    "Abort current run?", JOptionPane.YES_NO_OPTION);
            if(n != JOptionPane.OK_OPTION) {
                return;
            }
        }
        System.exit(0);
    }
}