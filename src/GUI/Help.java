package GUI;

import java.awt.Toolkit;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.event.HyperlinkEvent;

/**
 * The help window for assembler help. This window works as a very basic html file browser.
 * 
 * @author Milan Gritta
 */
public final class Help extends javax.swing.JFrame {

    /** 
     * Creates new form Help and sets the custom window icon. It also opens the help.html
     * file in the editor pane as the default view.
     * @param win 
     */
    public Help(JFrame win) {
        initComponents();
        editorPane.setEditable(false);
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icon.png")));
        setLocationRelativeTo(win);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrollPane = new javax.swing.JScrollPane();
        editorPane = new javax.swing.JEditorPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("BM Assembler Help - Sussex University");

        editorPane.setFont(new java.awt.Font("Microsoft JhengHei", 0, 14)); // NOI18N
        editorPane.setPreferredSize(new java.awt.Dimension(680, 515));
        editorPane.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
            public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {
                editorPaneHyperlinkUpdate(evt);
            }
        });
        scrollPane.setViewportView(editorPane);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void editorPaneHyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {//GEN-FIRST:event_editorPaneHyperlinkUpdate
        if (evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            try {
                editorPane.setPage(evt.getURL());
            } catch (IOException ex) {
                editorPane.setText("Sorry, can't open the requested file. Please report the fault to the maintaining programmer.");
            }
        }
    }//GEN-LAST:event_editorPaneHyperlinkUpdate

    /**
     * Enables the user to browse the HTML documentation pages.
     * 
     * @param filename to bea read and displayed
     */
    protected void setPage(String filename) {
        try {            
            editorPane.setPage(getClass().getResource("/" + filename));
        } catch (IOException ex) {
            editorPane.setText("Sorry, can't open the help file. It's likely been deleted or moved to a new location. Please report the fault to the maintaining programmer.");
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane editorPane;
    private javax.swing.JScrollPane scrollPane;
    // End of variables declaration//GEN-END:variables
}