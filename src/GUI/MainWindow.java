package GUI;

import MainEngine.BM;
import MainEngine.Executor;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;

/**
 * The main window class and GUI. This object handles the bulk of the operations and is the main
 * container for all the controls. It is a look-a-like of the Matlab BM written at Sussex University.
 * 
 * @author Milan Gritta
 */
public final class MainWindow extends javax.swing.JFrame {
        
    private static ActionHandler actionHandler;
    private static Instructions instructions;
    private static Help help;

    /** 
     * Creates new form MainWindow, sets a custom window icon and centres
     * the frame on the screen.
     */
    public MainWindow() {
        initComponents();
        actionHandler = new ActionHandler(mainMemoryTable, cpuTable);
        actionHandler.setColumnWidth();
        setLocationRelativeTo(null);
        instructions = new Instructions(this);
        help = new Help(this);
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icon.png")));
        this.setMnemonicKeys();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainMemoryScrollPane = new javax.swing.JScrollPane();
        mainMemoryTable = new javax.swing.JTable();
        cpuScrollPane = new javax.swing.JScrollPane();
        cpuTable = new javax.swing.JTable();
        listInstructionsButton = new javax.swing.JButton();
        helpButton = new javax.swing.JButton();
        assemblerHelpButton = new javax.swing.JButton();
        messageAreaScrollPane = new javax.swing.JScrollPane();
        messageArea = new javax.swing.JTextArea();
        stepButton = new javax.swing.JButton();
        continueButton = new javax.swing.JButton();
        resetRunbutton = new javax.swing.JButton();
        speedSlider = new javax.swing.JSlider();
        speedLabel = new javax.swing.JLabel();
        informationLabel = new javax.swing.JLabel();
        registersLabel = new javax.swing.JLabel();
        memoryLabel = new javax.swing.JLabel();
        cpuControlsLabel = new javax.swing.JLabel();
        resetButton = new javax.swing.JButton();
        saveInFileButton = new javax.swing.JButton();
        loadFromFileButton = new javax.swing.JButton();
        assebleLoadButton = new javax.swing.JButton();
        assembleFileButton = new javax.swing.JButton();
        descriptiveRadioButton = new javax.swing.JRadioButton();
        assemblerRadioButton = new javax.swing.JRadioButton();
        saveDisplayButton = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        counterLabel = new javax.swing.JLabel();
        counter = new javax.swing.JTextField();
        messagesLabel = new javax.swing.JLabel();
        resetRegistersButton = new javax.swing.JButton();
        bitmapDisplay = new BitmapDisplay();
        alexLogo = new javax.swing.JLabel();
        displayButton = new javax.swing.JToggleButton();
        quickCell = new javax.swing.JTextField();
        backButton = new javax.swing.JButton();
        aboutButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("JBrookshearMachine 1.6 - Sussex Univerisity");
        setFocusCycleRoot(false);
        setIconImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icon.png")));
        setMaximumSize(new java.awt.Dimension(1500, 1000));
        setMinimumSize(new java.awt.Dimension(994, 695));
        setName("mainWindow"); // NOI18N

        mainMemoryScrollPane.setPreferredSize(new java.awt.Dimension(469, 423));

        mainMemoryTable.setBackground(new java.awt.Color(232, 234, 241));
        mainMemoryTable.setFont(new java.awt.Font("Microsoft JhengHei", 0, 13)); // NOI18N
        mainMemoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"00", "00000000", "00", null, "0", "0.000", null},
                {"01", "00000000", "00", null, "0", "0.000", null},
                {"02", "00000000", "00", null, "0", "0.000", null},
                {"03", "00000000", "00", null, "0", "0.000", null},
                {"04", "00000000", "00", null, "0", "0.000", null},
                {"05", "00000000", "00", null, "0", "0.000", null},
                {"06", "00000000", "00", null, "0", "0.000", null},
                {"07", "00000000", "00", null, "0", "0.000", null},
                {"08", "00000000", "00", null, "0", "0.000", null},
                {"09", "00000000", "00", null, "0", "0.000", null},
                {"0A", "00000000", "00", null, "0", "0.000", null},
                {"0B", "00000000", "00", null, "0", "0.000", null},
                {"0C", "00000000", "00", null, "0", "0.000", null},
                {"0D", "00000000", "00", null, "0", "0.000", null},
                {"0E", "00000000", "00", null, "0", "0.000", null},
                {"0F", "00000000", "00", null, "0", "0.000", null},
                {"10", "00000000", "00", null, "0", "0.000", null},
                {"11", "00000000", "00", null, "0", "0.000", null},
                {"12", "00000000", "00", null, "0", "0.000", null},
                {"13", "00000000", "00", null, "0", "0.000", null},
                {"14", "00000000", "00", null, "0", "0.000", null},
                {"15", "00000000", "00", null, "0", "0.000", null},
                {"16", "00000000", "00", null, "0", "0.000", null},
                {"17", "00000000", "00", null, "0", "0.000", null},
                {"18", "00000000", "00", null, "0", "0.000", null},
                {"19", "00000000", "00", null, "0", "0.000", null},
                {"1A", "00000000", "00", null, "0", "0.000", null},
                {"1B", "00000000", "00", null, "0", "0.000", null},
                {"1C", "00000000", "00", null, "0", "0.000", null},
                {"1D", "00000000", "00", null, "0", "0.000", null},
                {"1E", "00000000", "00", null, "0", "0.000", null},
                {"1F", "00000000", "00", null, "0", "0.000", null},
                {"20", "00000000", "00", null, "0", "0.000", null},
                {"21", "00000000", "00", null, "0", "0.000", null},
                {"22", "00000000", "00", null, "0", "0.000", null},
                {"23", "00000000", "00", null, "0", "0.000", null},
                {"24", "00000000", "00", null, "0", "0.000", null},
                {"25", "00000000", "00", null, "0", "0.000", null},
                {"26", "00000000", "00", null, "0", "0.000", null},
                {"27", "00000000", "00", null, "0", "0.000", null},
                {"28", "00000000", "00", null, "0", "0.000", null},
                {"29", "00000000", "00", null, "0", "0.000", null},
                {"2A", "00000000", "00", null, "0", "0.000", null},
                {"2B", "00000000", "00", null, "0", "0.000", null},
                {"2C", "00000000", "00", null, "0", "0.000", null},
                {"2D", "00000000", "00", null, "0", "0.000", null},
                {"2E", "00000000", "00", null, "0", "0.000", null},
                {"2F", "00000000", "00", null, "0", "0.000", null},
                {"30", "00000000", "00", null, "0", "0.000", null},
                {"31", "00000000", "00", null, "0", "0.000", null},
                {"32", "00000000", "00", null, "0", "0.000", null},
                {"33", "00000000", "00", null, "0", "0.000", null},
                {"34", "00000000", "00", null, "0", "0.000", null},
                {"35", "00000000", "00", null, "0", "0.000", null},
                {"36", "00000000", "00", null, "0", "0.000", null},
                {"37", "00000000", "00", null, "0", "0.000", null},
                {"38", "00000000", "00", null, "0", "0.000", null},
                {"39", "00000000", "00", null, "0", "0.000", null},
                {"3A", "00000000", "00", null, "0", "0.000", null},
                {"3B", "00000000", "00", null, "0", "0.000", null},
                {"3C", "00000000", "00", null, "0", "0.000", null},
                {"3D", "00000000", "00", null, "0", "0.000", null},
                {"3E", "00000000", "00", null, "0", "0.000", null},
                {"3F", "00000000", "00", null, "0", "0.000", null},
                {"40", "00000000", "00", null, "0", "0.000", null},
                {"41", "00000000", "00", null, "0", "0.000", null},
                {"42", "00000000", "00", null, "0", "0.000", null},
                {"43", "00000000", "00", null, "0", "0.000", null},
                {"44", "00000000", "00", null, "0", "0.000", null},
                {"45", "00000000", "00", null, "0", "0.000", null},
                {"46", "00000000", "00", null, "0", "0.000", null},
                {"47", "00000000", "00", null, "0", "0.000", null},
                {"48", "00000000", "00", null, "0", "0.000", null},
                {"49", "00000000", "00", null, "0", "0.000", null},
                {"4A", "00000000", "00", null, "0", "0.000", null},
                {"4B", "00000000", "00", null, "0", "0.000", null},
                {"4C", "00000000", "00", null, "0", "0.000", null},
                {"4D", "00000000", "00", null, "0", "0.000", null},
                {"4E", "00000000", "00", null, "0", "0.000", null},
                {"4F", "00000000", "00", null, "0", "0.000", null},
                {"50", "00000000", "00", null, "0", "0.000", null},
                {"51", "00000000", "00", null, "0", "0.000", null},
                {"52", "00000000", "00", null, "0", "0.000", null},
                {"53", "00000000", "00", null, "0", "0.000", null},
                {"54", "00000000", "00", null, "0", "0.000", null},
                {"55", "00000000", "00", null, "0", "0.000", null},
                {"56", "00000000", "00", null, "0", "0.000", null},
                {"57", "00000000", "00", null, "0", "0.000", null},
                {"58", "00000000", "00", null, "0", "0.000", null},
                {"59", "00000000", "00", null, "0", "0.000", null},
                {"5A", "00000000", "00", null, "0", "0.000", null},
                {"5B", "00000000", "00", null, "0", "0.000", null},
                {"5C", "00000000", "00", null, "0", "0.000", null},
                {"5D", "00000000", "00", null, "0", "0.000", null},
                {"5E", "00000000", "00", null, "0", "0.000", null},
                {"5F", "00000000", "00", null, "0", "0.000", null},
                {"60", "00000000", "00", null, "0", "0.000", null},
                {"61", "00000000", "00", null, "0", "0.000", null},
                {"62", "00000000", "00", null, "0", "0.000", null},
                {"63", "00000000", "00", null, "0", "0.000", null},
                {"64", "00000000", "00", null, "0", "0.000", null},
                {"65", "00000000", "00", null, "0", "0.000", null},
                {"66", "00000000", "00", null, "0", "0.000", null},
                {"67", "00000000", "00", null, "0", "0.000", null},
                {"68", "00000000", "00", null, "0", "0.000", null},
                {"69", "00000000", "00", null, "0", "0.000", null},
                {"6A", "00000000", "00", null, "0", "0.000", null},
                {"6B", "00000000", "00", null, "0", "0.000", null},
                {"6C", "00000000", "00", null, "0", "0.000", null},
                {"6D", "00000000", "00", null, "0", "0.000", null},
                {"6E", "00000000", "00", null, "0", "0.000", null},
                {"6F", "00000000", "00", null, "0", "0.000", null},
                {"70", "00000000", "00", null, "0", "0.000", null},
                {"71", "00000000", "00", null, "0", "0.000", null},
                {"72", "00000000", "00", null, "0", "0.000", null},
                {"73", "00000000", "00", null, "0", "0.000", null},
                {"74", "00000000", "00", null, "0", "0.000", null},
                {"75", "00000000", "00", null, "0", "0.000", null},
                {"76", "00000000", "00", null, "0", "0.000", null},
                {"77", "00000000", "00", null, "0", "0.000", null},
                {"78", "00000000", "00", null, "0", "0.000", null},
                {"79", "00000000", "00", null, "0", "0.000", null},
                {"7A", "00000000", "00", null, "0", "0.000", null},
                {"7B", "00000000", "00", null, "0", "0.000", null},
                {"7C", "00000000", "00", null, "0", "0.000", null},
                {"7D", "00000000", "00", null, "0", "0.000", null},
                {"7E", "00000000", "00", null, "0", "0.000", null},
                {"7F", "00000000", "00", null, "0", "0.000", null},
                {"80", "00000000", "00", null, "0", "0.000", null},
                {"81", "00000000", "00", null, "0", "0.000", null},
                {"82", "00000000", "00", null, "0", "0.000", null},
                {"83", "00000000", "00", null, "0", "0.000", null},
                {"84", "00000000", "00", null, "0", "0.000", null},
                {"85", "00000000", "00", null, "0", "0.000", null},
                {"86", "00000000", "00", null, "0", "0.000", null},
                {"87", "00000000", "00", null, "0", "0.000", null},
                {"88", "00000000", "00", null, "0", "0.000", null},
                {"89", "00000000", "00", null, "0", "0.000", null},
                {"8A", "00000000", "00", null, "0", "0.000", null},
                {"8B", "00000000", "00", null, "0", "0.000", null},
                {"8C", "00000000", "00", null, "0", "0.000", null},
                {"8D", "00000000", "00", null, "0", "0.000", null},
                {"8E", "00000000", "00", null, "0", "0.000", null},
                {"8F", "00000000", "00", null, "0", "0.000", null},
                {"90", "00000000", "00", null, "0", "0.000", null},
                {"91", "00000000", "00", null, "0", "0.000", null},
                {"92", "00000000", "00", null, "0", "0.000", null},
                {"93", "00000000", "00", null, "0", "0.000", null},
                {"94", "00000000", "00", null, "0", "0.000", null},
                {"95", "00000000", "00", null, "0", "0.000", null},
                {"96", "00000000", "00", null, "0", "0.000", null},
                {"97", "00000000", "00", null, "0", "0.000", null},
                {"98", "00000000", "00", null, "0", "0.000", null},
                {"99", "00000000", "00", null, "0", "0.000", null},
                {"9A", "00000000", "00", null, "0", "0.000", null},
                {"9B", "00000000", "00", null, "0", "0.000", null},
                {"9C", "00000000", "00", null, "0", "0.000", null},
                {"9D", "00000000", "00", null, "0", "0.000", null},
                {"9E", "00000000", "00", null, "0", "0.000", null},
                {"9F", "00000000", "00", null, "0", "0.000", null},
                {"A0", "00000000", "00", null, "0", "0.000", null},
                {"A1", "00000000", "00", null, "0", "0.000", null},
                {"A2", "00000000", "00", null, "0", "0.000", null},
                {"A3", "00000000", "00", null, "0", "0.000", null},
                {"A4", "00000000", "00", null, "0", "0.000", null},
                {"A5", "00000000", "00", null, "0", "0.000", null},
                {"A6", "00000000", "00", null, "0", "0.000", null},
                {"A7", "00000000", "00", null, "0", "0.000", null},
                {"A8", "00000000", "00", null, "0", "0.000", null},
                {"A9", "00000000", "00", null, "0", "0.000", null},
                {"AA", "00000000", "00", null, "0", "0.000", null},
                {"AB", "00000000", "00", null, "0", "0.000", null},
                {"AC", "00000000", "00", null, "0", "0.000", null},
                {"AD", "00000000", "00", null, "0", "0.000", null},
                {"AE", "00000000", "00", null, "0", "0.000", null},
                {"AF", "00000000", "00", null, "0", "0.000", null},
                {"B0", "00000000", "00", null, "0", "0.000", null},
                {"B1", "00000000", "00", null, "0", "0.000", null},
                {"B2", "00000000", "00", null, "0", "0.000", null},
                {"B3", "00000000", "00", null, "0", "0.000", null},
                {"B4", "00000000", "00", null, "0", "0.000", null},
                {"B5", "00000000", "00", null, "0", "0.000", null},
                {"B6", "00000000", "00", null, "0", "0.000", null},
                {"B7", "00000000", "00", null, "0", "0.000", null},
                {"B8", "00000000", "00", null, "0", "0.000", null},
                {"B9", "00000000", "00", null, "0", "0.000", null},
                {"BA", "00000000", "00", null, "0", "0.000", null},
                {"BB", "00000000", "00", null, "0", "0.000", null},
                {"BC", "00000000", "00", null, "0", "0.000", null},
                {"BD", "00000000", "00", null, "0", "0.000", null},
                {"BE", "00000000", "00", null, "0", "0.000", null},
                {"BF", "00000000", "00", null, "0", "0.000", null},
                {"C0", "00000000", "00", null, "0", "0.000", null},
                {"C1", "00000000", "00", null, "0", "0.000", null},
                {"C2", "00000000", "00", null, "0", "0.000", null},
                {"C3", "00000000", "00", null, "0", "0.000", null},
                {"C4", "00000000", "00", null, "0", "0.000", null},
                {"C5", "00000000", "00", null, "0", "0.000", null},
                {"C6", "00000000", "00", null, "0", "0.000", null},
                {"C7", "00000000", "00", null, "0", "0.000", null},
                {"C8", "00000000", "00", null, "0", "0.000", null},
                {"C9", "00000000", "00", null, "0", "0.000", null},
                {"CA", "00000000", "00", null, "0", "0.000", null},
                {"CB", "00000000", "00", null, "0", "0.000", null},
                {"CC", "00000000", "00", null, "0", "0.000", null},
                {"CD", "00000000", "00", null, "0", "0.000", null},
                {"CE", "00000000", "00", null, "0", "0.000", null},
                {"CF", "00000000", "00", null, "0", "0.000", null},
                {"D0", "00000000", "00", null, "0", "0.000", null},
                {"D1", "00000000", "00", null, "0", "0.000", null},
                {"D2", "00000000", "00", null, "0", "0.000", null},
                {"D3", "00000000", "00", null, "0", "0.000", null},
                {"D4", "00000000", "00", null, "0", "0.000", null},
                {"D5", "00000000", "00", null, "0", "0.000", null},
                {"D6", "00000000", "00", null, "0", "0.000", null},
                {"D7", "00000000", "00", null, "0", "0.000", null},
                {"D8", "00000000", "00", null, "0", "0.000", null},
                {"D9", "00000000", "00", null, "0", "0.000", null},
                {"DA", "00000000", "00", null, "0", "0.000", null},
                {"DB", "00000000", "00", null, "0", "0.000", null},
                {"DC", "00000000", "00", null, "0", "0.000", null},
                {"DD", "00000000", "00", null, "0", "0.000", null},
                {"DE", "00000000", "00", null, "0", "0.000", null},
                {"DF", "00000000", "00", null, "0", "0.000", null},
                {"E0", "00000000", "00", null, "0", "0.000", null},
                {"E1", "00000000", "00", null, "0", "0.000", null},
                {"E2", "00000000", "00", null, "0", "0.000", null},
                {"E3", "00000000", "00", null, "0", "0.000", null},
                {"E4", "00000000", "00", null, "0", "0.000", null},
                {"E5", "00000000", "00", null, "0", "0.000", null},
                {"E6", "00000000", "00", null, "0", "0.000", null},
                {"E7", "00000000", "00", null, "0", "0.000", null},
                {"E8", "00000000", "00", null, "0", "0.000", null},
                {"E9", "00000000", "00", null, "0", "0.000", null},
                {"EA", "00000000", "00", null, "0", "0.000", null},
                {"EB", "00000000", "00", null, "0", "0.000", null},
                {"EC", "00000000", "00", null, "0", "0.000", null},
                {"ED", "00000000", "00", null, "0", "0.000", null},
                {"EE", "00000000", "00", null, "0", "0.000", null},
                {"EF", "00000000", "00", null, "0", "0.000", null},
                {"F0", "00000000", "00", null, "0", "0.000", null},
                {"F1", "00000000", "00", null, "0", "0.000", null},
                {"F2", "00000000", "00", null, "0", "0.000", null},
                {"F3", "00000000", "00", null, "0", "0.000", null},
                {"F4", "00000000", "00", null, "0", "0.000", null},
                {"F5", "00000000", "00", null, "0", "0.000", null},
                {"F6", "00000000", "00", null, "0", "0.000", null},
                {"F7", "00000000", "00", null, "0", "0.000", null},
                {"F8", "00000000", "00", null, "0", "0.000", null},
                {"F9", "00000000", "00", null, "0", "0.000", null},
                {"FA", "00000000", "00", null, "0", "0.000", null},
                {"FB", "00000000", "00", null, "0", "0.000", null},
                {"FC", "00000000", "00", null, "0", "0.000", null},
                {"FD", "00000000", "00", null, "0", "0.000", null},
                {"FE", "00000000", "00", null, "0", "0.000", null},
                {"FF", "00000000", "00", null, "0", "0.000", null}
            },
            new String [] {
                "Address", "Binary", "Hex", "ASCII", "Integer", "Float", "Instruction"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        mainMemoryTable.setToolTipText("Main memory - RAM (256 cells of 1 byte each)");
        mainMemoryTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        mainMemoryTable.setCellSelectionEnabled(true);
        mainMemoryTable.setDoubleBuffered(true);
        mainMemoryTable.setGridColor(new java.awt.Color(255, 255, 255));
        mainMemoryTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        mainMemoryTable.setShowHorizontalLines(false);
        mainMemoryTable.setShowVerticalLines(false);
        mainMemoryTable.getTableHeader().setResizingAllowed(false);
        mainMemoryTable.getTableHeader().setReorderingAllowed(false);
        mainMemoryTable.setUpdateSelectionOnSort(false);
        mainMemoryTable.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                mainMemoryTablePropertyChange(evt);
            }
        });
        mainMemoryScrollPane.setViewportView(mainMemoryTable);
        mainMemoryTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        cpuScrollPane.setBorder(null);

        cpuTable.setBackground(new java.awt.Color(235, 255, 235));
        cpuTable.setFont(new java.awt.Font("Microsoft JhengHei", 0, 13)); // NOI18N
        cpuTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"0", "00000000", "00", null, "0", "0.000"},
                {"1", "00000000", "00", null, "0", "0.000"},
                {"2", "00000000", "00", null, "0", "0.000"},
                {"3", "00000000", "00", null, "0", "0.000"},
                {"4", "00000000", "00", null, "0", "0.000"},
                {"5", "00000000", "00", null, "0", "0.000"},
                {"6", "00000000", "00", null, "0", "0.000"},
                {"7", "00000000", "00", null, "0", "0.000"},
                {"8", "00000000", "00", null, "0", "0.000"},
                {"9", "00000000", "00", null, "0", "0.000"},
                {"A", "00000000", "00", null, "0", "0.000"},
                {"B", "00000000", "00", null, "0", "0.000"},
                {"C", "00000000", "00", null, "0", "0.000"},
                {"D", "00000000", "00", null, "0", "0.000"},
                {"E", "00000000", "00", null, "0", "0.000"},
                {"F", "00000000", "00", null, "0", "0.000"}
            },
            new String [] {
                "Register", "Binary", "Hex", "ASCII", "Integer", "Float"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        cpuTable.setToolTipText("CPU registers (16 cells of 1 byte each)");
        cpuTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        cpuTable.setFocusable(false);
        cpuTable.setGridColor(new java.awt.Color(255, 255, 255));
        cpuTable.setMaximumSize(new java.awt.Dimension(2147483647, 260));
        cpuTable.setRowSelectionAllowed(false);
        cpuTable.setShowHorizontalLines(false);
        cpuTable.setShowVerticalLines(false);
        cpuScrollPane.setViewportView(cpuTable);

        listInstructionsButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        listInstructionsButton.setText("Instructions");
        listInstructionsButton.setToolTipText("Opens window with BM machine instructions");
        listInstructionsButton.setPreferredSize(new java.awt.Dimension(75, 25));
        listInstructionsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listInstructionsButtonActionPerformed(evt);
            }
        });

        helpButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        helpButton.setText("Help");
        helpButton.setToolTipText("Opens the help file window");
        helpButton.setMaximumSize(new java.awt.Dimension(95, 25));
        helpButton.setPreferredSize(new java.awt.Dimension(75, 25));
        helpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpButtonActionPerformed(evt);
            }
        });

        assemblerHelpButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        assemblerHelpButton.setText("Assembler help");
        assemblerHelpButton.setToolTipText("Opens the assembler help window");
        assemblerHelpButton.setIconTextGap(0);
        assemblerHelpButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        assemblerHelpButton.setMaximumSize(new java.awt.Dimension(95, 25));
        assemblerHelpButton.setMinimumSize(new java.awt.Dimension(95, 25));
        assemblerHelpButton.setPreferredSize(new java.awt.Dimension(75, 25));
        assemblerHelpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assemblerHelpButtonActionPerformed(evt);
            }
        });

        messageAreaScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        messageArea.setEditable(false);
        messageArea.setBackground(new java.awt.Color(255, 255, 217));
        messageArea.setColumns(20);
        messageArea.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        messageArea.setForeground(new java.awt.Color(255, 0, 0));
        messageArea.setLineWrap(true);
        messageArea.setRows(5);
        messageArea.setToolTipText("Information/message panel");
        messageArea.setWrapStyleWord(true);
        messageArea.setFocusable(false);
        messageAreaScrollPane.setViewportView(messageArea);

        stepButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        stepButton.setText("Do Step");
        stepButton.setToolTipText("Executes one step at a time");
        stepButton.setPreferredSize(new java.awt.Dimension(75, 25));
        stepButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepButtonActionPerformed(evt);
            }
        });

        continueButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        continueButton.setText("Continue");
        continueButton.setToolTipText("Pause/resume execution");
        continueButton.setMaximumSize(new java.awt.Dimension(75, 25));
        continueButton.setMinimumSize(new java.awt.Dimension(75, 25));
        continueButton.setPreferredSize(new java.awt.Dimension(75, 25));
        continueButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                continueButtonActionPerformed(evt);
            }
        });

        resetRunbutton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        resetRunbutton.setText("Reset & Run");
        resetRunbutton.setToolTipText("Resets the CPU registers and starts execution");
        resetRunbutton.setMaximumSize(new java.awt.Dimension(75, 25));
        resetRunbutton.setMinimumSize(new java.awt.Dimension(75, 25));
        resetRunbutton.setPreferredSize(new java.awt.Dimension(75, 25));
        resetRunbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetRunbuttonActionPerformed(evt);
            }
        });

        speedSlider.setFont(new java.awt.Font("Microsoft JhengHei", 0, 8)); // NOI18N
        speedSlider.setMajorTickSpacing(25);
        speedSlider.setMinorTickSpacing(5);
        speedSlider.setPaintLabels(true);
        speedSlider.setPaintTicks(true);
        speedSlider.setSnapToTicks(true);
        speedSlider.setToolTipText("Adjust speed of execution. Use RIGHT, LEFT arrow keys");
        speedSlider.setMinimumSize(new java.awt.Dimension(117, 42));
        speedSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                speedSliderStateChanged(evt);
            }
        });

        speedLabel.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        speedLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        speedLabel.setText("Speed");

        informationLabel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 12)); // NOI18N
        informationLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        informationLabel.setText("Information");

        registersLabel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 12)); // NOI18N
        registersLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        registersLabel.setText("Registers");

        memoryLabel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 12)); // NOI18N
        memoryLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        memoryLabel.setText("Memory");

        cpuControlsLabel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 12)); // NOI18N
        cpuControlsLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cpuControlsLabel.setText("CPU controls");

        resetButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        resetButton.setText("Reset");
        resetButton.setToolTipText("Resets main memory");
        resetButton.setBorder(null);
        resetButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        resetButton.setMaximumSize(new java.awt.Dimension(123, 125));
        resetButton.setPreferredSize(new java.awt.Dimension(63, 59));
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        saveInFileButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        saveInFileButton.setText("Save in file");
        saveInFileButton.setToolTipText("Saves contents of main memory to text file as machine code");
        saveInFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveInFileButtonActionPerformed(evt);
            }
        });

        loadFromFileButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        loadFromFileButton.setText("Load from file");
        loadFromFileButton.setToolTipText("Load machine code from a text file");
        loadFromFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadFromFileButtonActionPerformed(evt);
            }
        });

        assebleLoadButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        assebleLoadButton.setText("Assemble and load");
        assebleLoadButton.setToolTipText("Convert and load assembly code into main memory");
        assebleLoadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assebleLoadButtonActionPerformed(evt);
            }
        });

        assembleFileButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        assembleFileButton.setText("Assemble to file");
        assembleFileButton.setToolTipText("Convert assembly code to machine code and save to file");
        assembleFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assembleFileButtonActionPerformed(evt);
            }
        });

        descriptiveRadioButton.setFont(assebleLoadButton.getFont());
        descriptiveRadioButton.setSelected(true);
        descriptiveRadioButton.setText("descriptive");
        descriptiveRadioButton.setToolTipText("Show descriptive comments");
        descriptiveRadioButton.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        descriptiveRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descriptiveRadioButtonActionPerformed(evt);
            }
        });

        assemblerRadioButton.setFont(assebleLoadButton.getFont());
        assemblerRadioButton.setText("assembler ");
        assemblerRadioButton.setToolTipText("Show assembler comments");
        assemblerRadioButton.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        assemblerRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assemblerRadioButtonActionPerformed(evt);
            }
        });

        saveDisplayButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        saveDisplayButton.setText("Save Display");
        saveDisplayButton.setToolTipText("Saves bitmap display as a png image");
        saveDisplayButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveDisplayButtonActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        counterLabel.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        counterLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        counterLabel.setText("Program counter:");
        counterLabel.setToolTipText("Pointing to main memory (the next instruction location)");
        counterLabel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        counterLabel.setOpaque(true);

        counter.setBackground(new java.awt.Color(235, 255, 255));
        counter.setFont(new java.awt.Font("Microsoft JhengHei", 0, 22)); // NOI18N
        counter.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        counter.setText("00");
        counter.setToolTipText("Click to change execution start cell");
        counter.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        counter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                counterActionPerformed(evt);
            }
        });
        counter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                counterKeyReleased(evt);
            }
        });

        messagesLabel.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        messagesLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        messagesLabel.setText("Messages");

        resetRegistersButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        resetRegistersButton.setText("Reset");
        resetRegistersButton.setToolTipText("Resets the CPU registers");
        resetRegistersButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetRegistersButtonActionPerformed(evt);
            }
        });

        bitmapDisplay.setBackground(new java.awt.Color(153, 153, 153));
        bitmapDisplay.setForeground(new java.awt.Color(153, 153, 153));
        bitmapDisplay.setToolTipText("Graphical representation of cells 128-255");
        bitmapDisplay.setDoubleBuffered(false);
        bitmapDisplay.setEnabled(false);
        bitmapDisplay.setFocusable(false);
        bitmapDisplay.setMaximumSize(new java.awt.Dimension(823, 789));
        bitmapDisplay.setMinimumSize(new java.awt.Dimension(196, 180));
        bitmapDisplay.setPreferredSize(new java.awt.Dimension(196, 196));

        javax.swing.GroupLayout bitmapDisplayLayout = new javax.swing.GroupLayout(bitmapDisplay);
        bitmapDisplay.setLayout(bitmapDisplayLayout);
        bitmapDisplayLayout.setHorizontalGroup(
            bitmapDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        bitmapDisplayLayout.setVerticalGroup(
            bitmapDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        alexLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        alexLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon.png"))); // NOI18N
        alexLogo.setToolTipText("Image by Alex Jeffery");

        displayButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        displayButton.setText("Display On");
        displayButton.setToolTipText("Turn On/Off bitmap display below");
        displayButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayButtonActionPerformed(evt);
            }
        });

        quickCell.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        quickCell.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        quickCell.setText("Jump to cell...");
        quickCell.setToolTipText("Quickly scroll to a specified cell number");
        quickCell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quickCellActionPerformed(evt);
            }
        });
        quickCell.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                quickCellFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                quickCellFocusLost(evt);
            }
        });

        backButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 12)); // NOI18N
        backButton.setText("Undo Step");
        backButton.setToolTipText("Undo up to 500 steps");
        backButton.setMaximumSize(new java.awt.Dimension(75, 25));
        backButton.setMinimumSize(new java.awt.Dimension(75, 25));
        backButton.setPreferredSize(new java.awt.Dimension(75, 25));
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        aboutButton.setFont(new java.awt.Font("Microsoft JhengHei", 0, 11)); // NOI18N
        aboutButton.setText("About");
        aboutButton.setToolTipText("BSD Licence and Credits");
        aboutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(memoryLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(resetButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(saveInFileButton, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                            .addComponent(loadFromFileButton, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(assebleLoadButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(assembleFileButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(aboutButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(quickCell, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descriptiveRadioButton, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                            .addComponent(assemblerRadioButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(mainMemoryScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 477, Short.MAX_VALUE))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(bitmapDisplay, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE)
                            .addComponent(cpuScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(saveDisplayButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(counterLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, Short.MAX_VALUE)
                                .addComponent(counter, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(displayButton, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(resetRegistersButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(resetRunbutton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(continueButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(backButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(stepButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cpuControlsLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(0, 1, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(helpButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(assemblerHelpButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(messageAreaScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                                            .addComponent(listInstructionsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(informationLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(messagesLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addGap(11, 11, 11))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(alexLogo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(speedSlider, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addComponent(speedLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)))
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(registersLabel)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jSeparator3))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(helpButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(listInstructionsButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(45, 45, 45)
                        .addComponent(messagesLabel)
                        .addGap(3, 3, 3)
                        .addComponent(messageAreaScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cpuControlsLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stepButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(continueButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(resetRunbutton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(13, 13, 13)
                                .addComponent(speedLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(speedSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(alexLogo)))
                .addGap(12, 12, 12))
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(memoryLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(mainMemoryScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 541, Short.MAX_VALUE)
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(resetButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descriptiveRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(assebleLoadButton)
                                .addComponent(saveInFileButton)
                                .addComponent(aboutButton)))
                        .addGap(5, 5, 5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(assemblerRadioButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(assembleFileButton)
                                .addComponent(loadFromFileButton)
                                .addComponent(quickCell, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(informationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(87, 87, 87)
                        .addComponent(assemblerHelpButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(registersLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cpuScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(resetRegistersButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(counterLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(displayButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(saveDisplayButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(counter))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bitmapDisplay, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        actionHandler.resetMainMemory();
        bitmapDisplay.repaint();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void resetRegistersButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetRegistersButtonActionPerformed
        actionHandler.resetRegisters();
        mainMemoryTable.scrollRectToVisible(mainMemoryTable.getCellRect(00, 1, true));
    }//GEN-LAST:event_resetRegistersButtonActionPerformed

    private void mainMemoryTablePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_mainMemoryTablePropertyChange
        try { if(actionHandler != null) {  actionHandler.memoryPropertyChange(evt); }  } 
        catch(Exception e) { BM.log(e); }
    }//GEN-LAST:event_mainMemoryTablePropertyChange

    private void descriptiveRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descriptiveRadioButtonActionPerformed
        assemblerRadioButton.setSelected(!assemblerRadioButton.isSelected());
        actionHandler.switchMode();
    }//GEN-LAST:event_descriptiveRadioButtonActionPerformed

    private void assemblerRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assemblerRadioButtonActionPerformed
        descriptiveRadioButton.setSelected(!descriptiveRadioButton.isSelected());
        actionHandler.switchMode();
    }//GEN-LAST:event_assemblerRadioButtonActionPerformed

    private void listInstructionsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listInstructionsButtonActionPerformed
        instructions.setLocationRelativeTo(this);
        instructions.setVisible(true);
    }//GEN-LAST:event_listInstructionsButtonActionPerformed
    
    private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpButtonActionPerformed
        help.setPage("bmhelp.html"); help.setVisible(true);
    }//GEN-LAST:event_helpButtonActionPerformed

    private void assemblerHelpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assemblerHelpButtonActionPerformed
        help.setPage("help.html"); help.setVisible(true);
    }//GEN-LAST:event_assemblerHelpButtonActionPerformed

    private void loadFromFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadFromFileButtonActionPerformed
        try {actionHandler.loadFromFileButtonPressed(this); } 
        catch(Exception e) { BM.log(e); }
    }//GEN-LAST:event_loadFromFileButtonActionPerformed

    private void saveInFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveInFileButtonActionPerformed
        actionHandler.saveInFileButtonPressed(this);
    }//GEN-LAST:event_saveInFileButtonActionPerformed

    private void assembleFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assembleFileButtonActionPerformed
        try { actionHandler.assembleFileButtonPressed(this); } 
        catch(Exception e) { BM.log(e); }
    }//GEN-LAST:event_assembleFileButtonActionPerformed

    private void assebleLoadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assebleLoadButtonActionPerformed
        try { actionHandler.assebleLoadButtonPressed(this); } 
        catch(Exception e) { BM.log(e); }
    }//GEN-LAST:event_assebleLoadButtonActionPerformed

    private void stepButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stepButtonActionPerformed
        actionHandler.stepButtonPressed();
    }//GEN-LAST:event_stepButtonActionPerformed

    private void speedSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_speedSliderStateChanged
        JSlider source = (JSlider)evt.getSource();
        if (!source.getValueIsAdjusting()) { Executor.setSleepingTime((int)source.getValue()); }
    }//GEN-LAST:event_speedSliderStateChanged

    private void resetRunbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetRunbuttonActionPerformed
        actionHandler.resetRunbuttonPressed();
    }//GEN-LAST:event_resetRunbuttonActionPerformed

    private void continueButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_continueButtonActionPerformed
        actionHandler.continueButtonPressed();
    }//GEN-LAST:event_continueButtonActionPerformed

private void saveDisplayButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveDisplayButtonActionPerformed
        actionHandler.saveBitmapAsImage(bitmapDisplay, this);
}//GEN-LAST:event_saveDisplayButtonActionPerformed

    private void displayButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_displayButtonActionPerformed
        bitmapDisplay.setEnabled(displayButton.isSelected());
        if(displayButton.isSelected()) {
            displayButton.setText("Display Off");
        } 
        else {
            displayButton.setText("Display On");
        }
        if(bitmapDisplay.isEnabled()) { 
            bitmapDisplay.setBackground(Color.BLACK); 
        } 
        else { 
            bitmapDisplay.setBackground(new Color(153,153,153)); 
        }
    }//GEN-LAST:event_displayButtonActionPerformed

    private void quickCellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quickCellActionPerformed
        String text = this.quickCell.getText();
        try {
            int cell = Integer.parseInt(text,16);
            if(cell >= 0 && cell <= 255) {
                mainMemoryTable.scrollRectToVisible(mainMemoryTable.getCellRect(cell, 1, true));
                mainMemoryTable.repaint();
                cpuTable.repaint(); 
                mainMemoryTable.setColumnSelectionInterval(0, 6);
                mainMemoryTable.setRowSelectionInterval(cell, cell);
            }
            else {
                this.quickCell.setText("Out of range...");
            }
        }
        catch(Exception e) {
            this.quickCell.setText("Integers please...");
        }
    }//GEN-LAST:event_quickCellActionPerformed

    private void quickCellFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_quickCellFocusGained
        String text = this.quickCell.getText();
        if(text.equals("Jump to cell...")) {
            this.quickCell.setText("");
        }
    }//GEN-LAST:event_quickCellFocusGained

    private void quickCellFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_quickCellFocusLost
        this.quickCell.setText("Jump to cell...");
    }//GEN-LAST:event_quickCellFocusLost

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        actionHandler.backButtonPressed();
    }//GEN-LAST:event_backButtonActionPerformed

    private void aboutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutButtonActionPerformed
        new About(this).setVisible(true);
    }//GEN-LAST:event_aboutButtonActionPerformed

    private void counterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_counterActionPerformed
        try {
            int cell = Integer.parseInt(counter.getText(), 16);
            MainWindow.setCounter(counter.getText().toUpperCase(new Locale("en", "GB")));
            MainWindow.printText("");
            speedSlider.requestFocus();
            mainMemoryTable.scrollRectToVisible(mainMemoryTable.getCellRect(cell, 1, true));
            mainMemoryTable.setRowSelectionInterval(cell, cell);
            mainMemoryTable.setColumnSelectionInterval(0, 6);
        }
        catch(Exception e) {
            MainWindow.printText("Invalid counter start\nExpecting 1 or 2 hexadecimal characters...");
        }    
    }//GEN-LAST:event_counterActionPerformed

    private void counterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_counterKeyReleased
        if(counter.getText().length() > 2) {
            counter.setText(counter.getText().substring(1, 3).toUpperCase(new Locale("en", "GB")));
        }
    }//GEN-LAST:event_counterKeyReleased

    /**
     * Prints any text to message area using setText() erasing any previous messages.
     * @param text String message to print to the message area.
     */
    public static void printText(String text) {
        messageArea.setText(text);
        messageArea.setCaretPosition(0);
    }
    
    /**
     * Return the status of the assembler radio button (ON/OFF). It tells us whether to show
     * assembly code on screen or the more human friendly text (List instructions).
     * @return true if radio button is selected, false otherwise
     */
    public static boolean isAssembler() {
        return assemblerRadioButton.isSelected();
    }
    
    /**
     * Returns the contents of program counter. This value tells the executor object
     * where the next instruction to be executed is located.
     * @return value of the CPU counter as String.
     */
    public static String getCounter() {
        return counter.getText();
    }
    
    /**
     * Set the visible value of counter. Once an instruction had been executed, the executor object
     * sets the value on the GUI component making it available to view for the user.
     * @param value String hexadecimal number marking the current execution point/cell
     */
    public static void setCounter(String value) {
        counter.setText(value);
    }
    
    /**
     * Set the text of Continue/Stop button. This is used to change the text to Stop when Reset&Run button is
     * pressed and the executor object is working the instructions. When the user pauses execution by pressing
     * the Stop button, the text is changed to Continue.
     * @param text String text to be shown on the face of the Continue/Stop button
     */
    public static void setButtonText(String text) {
        continueButton.setText(text);
    }
    
    /**
     * Updates the bitmap display. This calls the repaint() instance method telling
     * the bitmap display to redraw itself.
     */
    public static void updateBitmap() {
        bitmapDisplay.repaint();
    }
    
    /**
     * WARNING; This method returns the size of the display minus any excess space
     * obliterated by resizing the component. This is not a getWidth and getHeight method!!
     * @return width and height of the VISIBLE part of this panel
     */
    protected static int [] getBitmapSize() {
        int [] size = new int [2];
        size[0] = bitmapDisplay.getWidth();
        size[1] = bitmapDisplay.getHeight();
        return size;
    }
    
    private void setMnemonicKeys() {
        this.aboutButton.setMnemonic(KeyEvent.VK_B);
        this.resetRunbutton.setMnemonic(KeyEvent.VK_R);
        this.stepButton.setMnemonic(KeyEvent.VK_S);
        this.listInstructionsButton.setMnemonic(KeyEvent.VK_I);
        this.helpButton.setMnemonic(KeyEvent.VK_H);
        this.displayButton.setMnemonic(KeyEvent.VK_N);
        this.assemblerHelpButton.setMnemonic(KeyEvent.VK_A);
        this.saveDisplayButton.setMnemonic(KeyEvent.VK_G);
        this.loadFromFileButton.setMnemonic(KeyEvent.VK_L);
        this.saveInFileButton.setMnemonic(KeyEvent.VK_V);
        this.assebleLoadButton.setMnemonic(KeyEvent.VK_D);
        this.assembleFileButton.setMnemonic(KeyEvent.VK_F);
        this.resetButton.setMnemonic(KeyEvent.VK_E);
        this.resetRegistersButton.setMnemonic(KeyEvent.VK_T);
        this.backButton.setMnemonic(KeyEvent.VK_U);
        InputMap inputMap = speedSlider.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(KeyStroke.getKeyStroke("RIGHT"), "right");
        inputMap.put(KeyStroke.getKeyStroke("LEFT"), "left");
        this.speedSlider.getActionMap().put("right", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
               speedSlider.setValue(speedSlider.getValue() + 5);
            }
        });
        this.speedSlider.getActionMap().put("left", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
               speedSlider.setValue(speedSlider.getValue() - 5); 
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent ev) {
                actionHandler.shutDown();
            }
        });
        continueButton.setMnemonic(KeyEvent.VK_C);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton aboutButton;
    private javax.swing.JLabel alexLogo;
    private javax.swing.JButton assebleLoadButton;
    private javax.swing.JButton assembleFileButton;
    private javax.swing.JButton assemblerHelpButton;
    private static javax.swing.JRadioButton assemblerRadioButton;
    private javax.swing.JButton backButton;
    private static javax.swing.JPanel bitmapDisplay;
    private static javax.swing.JButton continueButton;
    private static javax.swing.JTextField counter;
    private javax.swing.JLabel counterLabel;
    private javax.swing.JLabel cpuControlsLabel;
    private static javax.swing.JScrollPane cpuScrollPane;
    private static javax.swing.JTable cpuTable;
    private static javax.swing.JRadioButton descriptiveRadioButton;
    private javax.swing.JToggleButton displayButton;
    private javax.swing.JButton helpButton;
    private javax.swing.JLabel informationLabel;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JButton listInstructionsButton;
    private javax.swing.JButton loadFromFileButton;
    private static javax.swing.JScrollPane mainMemoryScrollPane;
    private static javax.swing.JTable mainMemoryTable;
    private javax.swing.JLabel memoryLabel;
    private static javax.swing.JTextArea messageArea;
    private javax.swing.JScrollPane messageAreaScrollPane;
    private javax.swing.JLabel messagesLabel;
    private javax.swing.JTextField quickCell;
    private javax.swing.JLabel registersLabel;
    private javax.swing.JButton resetButton;
    private javax.swing.JButton resetRegistersButton;
    private javax.swing.JButton resetRunbutton;
    private javax.swing.JButton saveDisplayButton;
    private javax.swing.JButton saveInFileButton;
    private javax.swing.JLabel speedLabel;
    private javax.swing.JSlider speedSlider;
    private javax.swing.JButton stepButton;
    // End of variables declaration//GEN-END:variables

    
    /**
     * This is the implementation of the bitmap display.
     * Essentially, we extend JPanel and override the method
     * which paints the panel adding some custom code.
     */
    private static class BitmapDisplay extends JPanel {
    
       @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            int cellWidth = this.getWidth() / 32;
            int cellHeight = this.getHeight() / 32;
            super.setSize(cellWidth * 32, cellHeight * 32);
            if(!bitmapDisplay.isEnabled()) { return; }
            for(int row = 128; row < 256; row++) {
                String bits = mainMemoryTable.getValueAt(row, 1).toString();
                if(bits.equals("00000000")) { continue; }
                for(int i = 0; i < 8; i++) {
                    int x =  (i * cellWidth) + (((row - 128) % 4) * (8 * cellWidth));
                    int y = ((row - 128) / 4) * cellHeight;
                    if(bits.charAt(i) == '1') {
                        g.setColor(Color.WHITE);
                        g.fillRect(x, y, cellWidth, cellHeight);
                    }
                    else {
                        g.setColor(Color.BLACK);
                        g.fillRect(x, y, cellWidth, cellHeight);
                    }
                }
            }
        }
    }
}
