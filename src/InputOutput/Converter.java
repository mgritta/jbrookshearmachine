package InputOutput;

import GUI.MainWindow;
import java.util.Locale;


/**
 * Converter class implementation. This class is responsible for all number base conversion
 * using the two static methods. It converts any value into the five appropriate bases/values: 
 * Binary, Hexadecimal, ASCII, Decimal and Float.
 * 
 * @author Milan Gritta
 */
public class Converter {

    private static String [] values = new String[5];
    /**
     * Denotes binary member of the array of values.
     */
    public static final int BIN = 0,
    /**
     * Denotes hexadecimal member of the array of values.
     */
    HEX = 1,
    /**
     * Denotes ASCII member of the array of values.
     */
    ASCII = 2,
    /**
     * Denotes decimal member of the array of values.
     */
    DEC = 3,
    /**
     * Denotes float member of the array of values.
     */
    FLOAT = 4;
    
    private Converter() {} //Only using static access
    
    /**
     * Converts any object into all 5 values. This static method takes the value to be converted
     * and the type of value and converts it into an array of all five values.
     * 
     * @param object the actual value to be converted
     * @param type this specifies the kind of value that is to be converted
     * @return values the array of Objects represented as [String,String,String,String,String]
     */
    public static String [] convert(Object object, int type) {
        try {
            switch(type) {
                case BIN:       binToAll(object);//String
                    break;
                case HEX:       hexToAll(object);//String
                    break;
                case ASCII:     asciiToAll(object);//String
                    break;
                case DEC:       decToAll(object);//String
                    break;
                case FLOAT:     floatToAll(object);//String
                    break;
                default: throw new RuntimeException("Converter:convert:Invalid Argument!");
            }
        }
        catch(NumberFormatException nfe) {
            MainWindow.printText("Illegal input:\n" + nfe.getMessage());
        }
        catch(IllegalArgumentException iae) {
            MainWindow.printText("Error:\n" + iae.getMessage());
        }
        return values();
    }

    private static void asciiToAll(Object object) throws IllegalArgumentException {
        String input = object.toString();
        if(input.isEmpty()) {
            decToAll(0);
            return;
        }
        if(input.length() > 1) {
            input = input.substring(input.length() - 1);
            MainWindow.printText("Only one character needed here...\nTrimming down to: " + input);
        }
        decToAll((int) input.charAt(0));       
    }
    
    private static void binToAll(Object object) throws IllegalArgumentException {
        String binary = object.toString();
        if(binary.length() < 1) {
            decToAll(0);
            return;
        }
        if(binary.length() > 8) {
            binary = binary.substring(binary.length() - 8);
            MainWindow.printText("Only 8 characters needed here...\nTrimming down to: " + binary);
        }
        if(!binary.matches("[01]+")) {
            decToAll(0);
            throw new IllegalArgumentException("Only binary characters allwed here...");
        }
        int decimal = Integer.parseInt(binToDec(binary));
        decimal = (decimal > 127) ? decimal - 256 : decimal;
        decToAll(decimal);
    }

    private static void hexToAll(Object object) throws IllegalArgumentException, NumberFormatException {
        String value = object.toString();
        if(value.length() < 1) {
            decToAll(0);
            return;
        }
        if(value.length() > 2) {
            value = value.substring(value.length() - 2);
            MainWindow.printText("Only 2 characters needed here...\nTrimming down to: " + value);
        }
        int finalValue = Integer.parseInt(value, 16);
        finalValue = (finalValue > 127) ? finalValue - 256 : finalValue;
        decToAll(finalValue);
    }

    private static void decToAll(Object object) throws IllegalArgumentException, NumberFormatException {
        String input = object.toString();
        input = (input.isEmpty()) ? "0" : input;
        int value = Integer.parseInt(input);
        if(value > 127 || value < -128) {
            decToAll(0);
            throw new IllegalArgumentException("Value " + value + " is out of range (-128 to 127)");
        }
        int eightBitValue = (value < 0) ? 256 + value : value;
        values[BIN] =   decToBin(eightBitValue);
        values[HEX] =   decToHex(eightBitValue);
        values[ASCII] = (eightBitValue > 31 && eightBitValue < 127) ? "" + (char) eightBitValue : null;
        values[DEC] =    "" + value;
        values[FLOAT] =  binToFloat("" + values[BIN]);
    }

    private static void floatToAll(Object o) {
        String floatString = o.toString();
        if(floatString.length() < 1) {
            decToAll(0);
            return;
        }
        float f = Float.parseFloat(floatString);
        if(f == 0) {
            binToAll("00000000");
            return;
        }
        String signBit = "0";
        if (f < 0) {
            signBit = "1";
            f = -f;
        }
        if(f < -7.5 || f > 7.5) {
            decToAll(0);
            throw new RuntimeException("Float outside range (-7.5 to 7.5)");
        }
        int integerPart = (int) f;
        float fractionalPart = f - integerPart;
        fractionalPart *= 16;
        float tmp = fractionalPart;
        int factor = 16;
        int count = 0;
        while ((tmp - (int) tmp > 0) && (count < 4)) {
            factor = factor * 2;
            tmp = (f - integerPart) * factor;
            count++;
        }
        String fractionalBits = Integer.toBinaryString((int) tmp);
        count = count + 4 - fractionalBits.length();
        while (count > 4) {
            fractionalBits = "0" + fractionalBits;
            count--;
        }
        int moveLeft = -count;
        String integerBits = integerPart == 0 ? "" : Integer.toBinaryString(integerPart);
        String exp = integerBits.length() == 0 ? toBinaryString(moveLeft + 4, 3) : toBinaryString(integerBits.length() + 4, 3);
        String result = signBit + exp + integerBits + fractionalBits;
        while (result.length() < 8) {
            result = result + "0";
        }
        binToAll(result.substring(0, 8));
    }
    
    private static String toBinaryString(int value, int minLength) {
        StringBuilder sb = new StringBuilder(Integer.toBinaryString(value));
        while (sb.length() < minLength) {
            sb.insert(0, '0');
        }
        return sb.toString();
    }
    
    /**
     * Dec to hex conversion. Converts a decimal value
     * to hexadecimal value.
     * 
     * @param value to be converted (decimal, BASE 10)
     * @return hexadecimal equivalent
     */
    public static String decToHex(int value) {
        String hex = Integer.toHexString(value).toUpperCase(new Locale("en", "GB"));
        return (hex.length() == 1) ? "0" + hex : hex;
    }
    
    private static String decToBin(int value) {
        return toBinaryString(value, 8);
    }

    private static String binToFloat(String value) {
        String zeroes = "0000";
        String signBit = (value.charAt(0) == '1') ? "-" : "";
        int exp = Integer.parseInt(binToDec(value.substring(1, 4)));
        String left = (exp > 4) ? value.substring(4, exp) : "0";
        String right = (exp > 4) ? value.substring(exp, value.length()) : zeroes.substring(0, 4 - exp) 
                    + value.substring(4, value.length());
        left = binToDec(left);
        right = binToDecFraction(right);
        return signBit + left + right;
    }

    private static String binToDec(String value) {
        int i = 0, decimal = 0;
        int bitValue = (int) Math.pow(2.0, (double) value.length() - 1);
        while(i < value.length()) {
            if(value.charAt(i) == '1') {
                decimal += bitValue;
            }
            bitValue /= 2;
            i++;
        }
        return "" + decimal;
    }

    private static String binToDecFraction(String value) {
        double bitValue = 0.5, decimal = 0.0;
        int i = 0;
        while(i < value.length()) {
            if(value.charAt(i) == '1') {
                decimal += bitValue;
            }
            bitValue /= 2;
            i++;
        }
        String result = decimal + "0000";
        return result.substring(1, 5);
    }
    
    private static String [] values() {
        String [] copy = values.clone();
        values[0] = null;
        values[1] = null;
        values[2] = null;
        values[3] = null;
        values[4] = null;
        return copy;
    }
}