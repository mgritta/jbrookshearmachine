package InputOutput;

import GUI.MainWindow;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * File loader/interpreter class. This class handles all file input/output.
 * It also handles the interpretation of assembly code from text files.
 * 
 * @author Milan Gritta
 */
public class FileIO {
    /**
     * String containing the most recent directory accessed by user
     */
    public static String currentDirectory;
    private HashMap<String,String> labels;
    private ArrayList<Integer> incomplete;
    private int row = 0;
    
    /**
     * Empty default constructor. Nothing special happening here.
     */
    public FileIO() {
        incomplete = new ArrayList<>();
        labels = new HashMap<>();
    }
    
    /**
     * Read the machine code from file. Reads a file and loads an ArrayList with
     * the machine code from that file to be processed/displayed later.
     * 
     * @param file to be read containing machine code
     * @return list of hexadecimal machine code as Strings
     */
    public ArrayList<String> readMachineCode(File file) {
        ArrayList<String> list = new ArrayList<>();
        int lineNo = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String line;
            while ((line = in.readLine()) != null) {
                lineNo++;
                String instruction = processLineMachine(line);
                if(instruction == null) {
                    continue;
                }
                list.add(instruction);
            }
            in.close();
        } 
        catch (IOException e) {
            MainWindow.printText("Sorry, an error occurred reading " + file.toString());
            return null;
        }
        catch(RuntimeException re) {
            MainWindow.printText("Error on line " + lineNo + "\n" + re.getMessage());
            return null;
        }
        return list;
    }
    
    private String processLineMachine(String line) {
        char [] array = line.toCharArray();
        String hex = "";
        int i = 0;
        boolean colon = false;
        line = line.replace(" ", "").replace("\t", "");
        while(i < array.length && array[i] != '/') {
            if(array[i] == ':') {
                hex = "--" + hex;
                if(colon) {
                    throw new RuntimeException("Malformed instruction!\nPerhaps used 2 colon symbols?\nLine:" + line);
                }
                else {
                    colon = true;
                }
                i++;
                continue;
            }
            try {
                Integer.parseInt("" + array[i], 16);
            }
            catch(NumberFormatException e) {
                throw new RuntimeException("Error in line " + line + "\nNot a hexadecimal character!\n\n"
                                    + "Expecting machine code!");
            }
            hex += array[i];
            i++;
        }
        if(hex.isEmpty()) {
            return null;
        }
        else {
            return hex;
        }
    }
    
    /**
     * Saves the machine code to a text file. Takes an ArrayList of Strings and saves
     * the code to a text file in the location specified by the path argument.
     * @param list the list of machine code as Strings
     * @param path the absolute path as destination folder
     */
    public void saveCode(ArrayList<String> list, String path) {
        BufferedWriter output;
        try {
            File file = new File(path);
            output = new BufferedWriter(new FileWriter(file));
            output.write("//Brookshear Machine Emulator by Milan Gritta. Copyright Sussex University");
            output.newLine();
            output.write("//Saved on " + new Date().toString());
            output.newLine();
            for(int i = 0; i < list.size(); i++) {
                output.write(list.get(i));
                output.newLine();
            }
            output.close();
        } catch (IOException ex) {
            MainWindow.printText("Sorry, an arror occurred saving your code!\n" + ex.getMessage());
        }
    }
    
    /**
     * Converts assembly code to machine code. Reads the file containing
     * assembly code and converts that into machine code.
     * @param file the text file to be read
     * @return list of assembled code as Strings
     */
    public ArrayList<String> assembleCode(File file) {
        ArrayList<String> list = new ArrayList<>();
        labels.clear();
        incomplete.clear();
        int lineNo = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String line;
            row = -2;
            while ((line = in.readLine()) != null) {
                lineNo++;
                if(line.matches("[\\s]+")) {
                    continue;
                }
                String instruction = processLineAssembly(line);
                if(instruction == null) {
                    continue;
                }
                if(instruction.isEmpty()) {
                    throw new RuntimeException("Cannot read this line of assembly code:\n" 
                            + line + "\nLine number: " + lineNo);
                }
                if(instruction.startsWith("\nBM:")) { 
                    throw new RuntimeException("Error on line number: " + lineNo + "\n" 
                            + line + "\n" + instruction);
                }
                list.add(instruction + "   //  " + line);
            }
            in.close();
            for(Integer index: incomplete) {
                row = index - 2;
                String toReplace = getToBeReplaced(list);
                String result = processLineAssembly(toReplace);
                String lineText = toReplace.substring(toReplace.indexOf("//") + 2, toReplace.length());
                String replacement = result + "   //" + lineText;
                if(result.startsWith("!!")) {
                    throw new RuntimeException("Unknown label used!!\nThe offending line:\n" + lineText.trim());
                }
                setToBeReplaced(replacement, list);
            }
        }
        catch(RuntimeException r) {
            MainWindow.printText("Assembly Code Error:\n\n" + r.getMessage());
            return null;
        }
        catch (IOException e) {
            MainWindow.printText("Sorry, an error occurred reading " + file.getName());
        }
        return list;
    }
    
    private String processLineAssembly(String line) {
        if(line.isEmpty()) {
            return null;
        }
        if(line.matches("[\\s]*//.*")) {
            return null;
        }
        if(line.contains("//")) {
            line = line.substring(0, line.indexOf("//")).trim();
        }
        if(line.trim().matches(".{1,}\".{2,}\"")) {
            String dataString = line.substring(line.indexOf("\""), line.lastIndexOf("\"") + 1);
            return toMachineCode(line.substring(0, line.indexOf("\"")).replace(" ", "") + dataString);
        }
        return toMachineCode(line.replace("\t", "").replace(" ", ""));
    }
    
    private String toMachineCode(String line) {
        row += 2;
        String rowString = Converter.decToHex(row) + ": ";
        if(line.length() >= 3 && line.substring(0, 3).matches("[\\dA-Fa-f]{2}:")) {
            row = Integer.parseInt(line.substring(0, 2), 16);
            row -= 2;
            if(line.substring(line.indexOf(":"), line.length()).length() == 1) {
                return null;
            }
            else {
                return toMachineCode(line.substring(3, line.length()));
            }
        }
        if(line.matches("[Nn][Oo][Pp]")) {
            return rowString + "0FFF";
        }
        else if(line.matches("[Mm][Oo][Vv]\\[[\\dA-Fa-f]{2}[Hh]?\\]->[Rr][\\dA-Fa-f]")) {
            return rowString + "1" + line.charAt(10) + line.substring(4, 6);
        }
        else if(line.matches("[Mm][Oo][Vv][\\dA-Fa-f]->[Rr][\\dA-Fa-f]")) {
            return rowString + "2" + line.charAt(7) + "0" + line.charAt(3);
        }
        else if(line.matches("[Mm][Oo][Vv][\\dA-Fa-f]{2}[Hh]?->[Rr][\\dA-Fa-f]")) {
            return rowString + "2" + line.charAt(line.length() - 1) + line.substring(3, 5);
        }
        else if(line.matches("[Mm][Oo][Vv][Rr][\\dA-Fa-f]->\\[[\\dA-Fa-f]{2}\\]")) {
            return rowString + "3" + line.charAt(4) + line.substring(8, 10);
        }
        else if(line.matches("[Mm][Oo][Vv][Rr][\\dA-Fa-f]->[Rr][\\dA-Fa-f]")) {
            return rowString + "40" + line.charAt(4) + line.charAt(8);
        }
        else if(line.matches("[Aa][Dd][Dd][Ii][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]->[Rr][\\dA-Fa-f]")) {
            return rowString + "5" + line.charAt(12) + line.charAt(5) + line.charAt(8);
        }
        else if(line.matches("[Aa][Dd][Dd][Ff][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]->[Rr][\\dA-Fa-f]")) {
            return rowString + "6" + line.charAt(12) + line.charAt(5) + line.charAt(8);
        }
        else if(line.matches("[Oo][Rr][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]->[Rr][\\dA-Fa-f]")) {
            return rowString + "7" + line.charAt(10) + line.charAt(3) + line.charAt(6);
        }
        else if(line.matches("[Aa][Nn][Dd][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]->[Rr][\\dA-Fa-f]")) {
            return rowString + "8" + line.charAt(11) + line.charAt(4) + line.charAt(7);
        }
        else if(line.matches("[Xx][Oo][Rr][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]->[Rr][\\dA-Fa-f]")) {
            return rowString + "9" + line.charAt(11) + line.charAt(4) + line.charAt(7);
        }
        else if(line.matches("[Rr][Oo][Tt][Rr][\\dA-Fa-f],[\\dA-Fa-f]")) {
            return rowString + "A" + line.charAt(4) + "0" + line.charAt(6);
        }
        else if(line.matches("[Jj][Mm][Pp][Ee][Qq][\\dA-Fa-f]{2},[Rr][\\dA-Fa-f]")) {
            return rowString + "B" + line.charAt(9) + line.substring(5, 7);
        }
        else if(line.matches("[Jj][Mm][Pp][\\dA-Fa-f]{2}")) {
            return rowString + "B0" + line.substring(3, 5);
        }
        else if(line.matches("[Hh][Aa][Ll][Tt]")) {
            return rowString + "C000";
        }
        else if(line.matches("[Mm][Oo][Vv]\\[R[\\dA-Fa-f]\\]->[Rr][\\dA-Fa-f]")) {
            return rowString + "D0" + line.charAt(10) + line.charAt(5);
        }
        else if(line.matches("[Mm][Oo][Vv][Rr][\\dA-Fa-f]->\\[[Rr][\\dA-Fa-f]\\]")) {
            return rowString + "E0" + line.charAt(4) + line.charAt(9);
        }
        else if(line.matches("[Jj][Mm][Pp][Ee][Qq][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]")) {
            return rowString + "F" + line.charAt(9) + "0" + line.charAt(6);
        }
        else if(line.matches("[Jj][Mm][Pp][Nn][Ee][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]")) {
            return rowString + "F" + line.charAt(9) + "1" + line.charAt(6);
        }
        else if(line.matches("[Jj][Mm][Pp][Gg][Ee][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]")) {
            return rowString + "F" + line.charAt(9) + "2" + line.charAt(6);
        }
        else if(line.matches("[Jj][Mm][Pp][Ll][Ee][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]")) {
            return rowString + "F" + line.charAt(9) + "3" + line.charAt(6);
        }
        else if(line.matches("[Jj][Mm][Pp][Gg][Tt][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]")) {
            return rowString + "F" + line.charAt(9) + "4" + line.charAt(6);
        }
        else if(line.matches("[Jj][Mm][Pp][Ll][Tt][Rr][\\dA-Fa-f],[Rr][\\dA-Fa-f]")) {
            return rowString + "F" + line.charAt(9) + "5" + line.charAt(6);
        }
        else if(line.matches("[Jj][Mm][Pp]\\w{1,}")) {
            String key = line.substring(3, line.length());
            if(labels.containsKey(key)) {
                return rowString + "B0" + labels.get(key);
            }
            else {
                incomplete.add(row);
                return "!!" + line;
            }
        }
        else if(line.matches("[Jj][Mm][Pp][Ee][Qq]\\w{1,},[Rr][\\dA-Fa-f]")) {
            int divider = line.indexOf(',');
            String key = line.substring(5, divider);
            if(labels.containsKey(key)) {
                return rowString + "B" + line.charAt(divider + 2) + labels.get(key);
            }
            else {
                incomplete.add(row);
                return "!!" + line;
            }
        }
        else if(line.matches("[Mm][Oo][Vv][01]{8}[Bb]?->[Rr][0-9a-fA-F]")) {
            return rowString + "2" + line.charAt(line.length() - 1) + Converter.convert(line.substring(3, 11), Converter.BIN)[Converter.HEX];
        }
        else if(line.matches("[Mm][Oo][Vv]\".\"->[Rr][0-9a-fA-F]")) {
            return rowString + "2" + line.charAt(line.length() - 1) + Converter.convert(line.charAt(4), Converter.ASCII)[Converter.HEX];
        }
        else if(line.matches("[Mm][Oo][Vv][-+]?\\d+\\.\\d+->[Rr][0-9a-fA-F]")) {
            String num = line.substring(3, line.lastIndexOf('-'));
            if(num.charAt(0) == '+') {
                num = num.substring(1);
            }
            float f = Float.parseFloat(num);
            if(f < -7.5 || f > 7.5) {
                throw new RuntimeException("Value " + num + " is out of range!!!");
            }
            return rowString + "2" + line.charAt(line.length() - 1) + Converter.convert(num, Converter.FLOAT)[Converter.HEX];
        }
        else if(line.matches("[Mm][Oo][Vv][+-]?\\d+->[Rr][0-9a-fA-F]")) {
            String num = line.substring(3, line.lastIndexOf('-'));
            if(num.charAt(0) == '+') {
                num = num.substring(1);
            }
            int integer = Integer.parseInt(num);
            if(integer < -128 || integer > 127) {
                throw new RuntimeException("Value " + num + " is out of range!!!");
            }
            return rowString + "2" + line.charAt(line.length() - 1) + Converter.convert(num, Converter.DEC)[Converter.HEX];
        }
        else if(line.matches("[Mm][Oo][Vv]\\w{1,}->[Rr][0-9a-fA-F]")) {
            String key = line.substring(3, line.length() - 4);
            if(labels.containsKey(key)) {
                return rowString + "2" + line.charAt(line.length() - 1) + labels.get(key);
            }
            else {
                incomplete.add(row);
                return "!!" + line;
            }
        }     
        else if(line.matches("[Mm][Oo][Vv][Rr][\\dA-Fa-f]->\\[\\w{1,}\\]")) {
            int bracketPos = line.indexOf("->");
            String key = line.substring(bracketPos + 3, line.length() - 1);
            if(labels.containsKey(key)) {
                return rowString + "3" + line.charAt(4) + labels.get(key);
            }
            else {
                incomplete.add(row);
                return "!!" + line;
            }
        }
        else if(line.matches("[A-Za-z]\\w{3,}:.{1,}")) {
            int divider = line.indexOf(":");
            String key = line.substring(0, divider);
            if(labels.containsKey(key)) {
                throw new RuntimeException("Duplicate label " + key + "\nNot allowed!!!!");
            }
            labels.put(key, Converter.decToHex(row));
            row -= 2;
            return toMachineCode(line.substring(divider + 1, line.length()));
        }
        else if(line.matches("[A-Za-z]\\w{3,}:")) {
            int divider = line.indexOf(":");
            String key = line.substring(0, divider);
            if(labels.containsKey(key)) {
                throw new RuntimeException("Duplicate label " + key + "\nNot allowed!!!!");
            }
            labels.put(key, Converter.decToHex(row));
            row -= 2;
            return null;
        }
        else if(line.matches("[Mm][Oo][Vv]\\[\\w{1,}\\]->[Rr][\\dA-Fa-f]")) {
            int bracketPos = line.indexOf(']');
            String key = line.substring(line.indexOf('[') + 1, bracketPos);
            if(labels.containsKey(key)) {
                return rowString + "1" + line.charAt(line.length() - 1) + labels.get(key);
            }
            else {
                incomplete.add(row);
                return "!!" + line;
            }
        }
        else if(line.matches("[Dd][Aa][Tt][Aa].{1,}")) {
            StringTokenizer st = new StringTokenizer(line.substring(4, line.length()),",");
            String codeArray = Converter.decToHex(row) + ": ";
            row -= 1;
            while(st.hasMoreElements()) {
                row++;
                String data = st.nextToken().trim();
                if(data.matches("\".{2,}\"")) {
                    data = data.substring(1, data.length() - 1);
                    for(int j = 0; j < data.length(); j++) {
                        codeArray += Converter.convert(data.charAt(j), Converter.ASCII)[Converter.HEX];
                        row++;
                    }
                    row--;
                    continue;
                }
                if(data.matches("\".\"")) {
                    codeArray += Converter.convert(data.charAt(1), Converter.ASCII)[Converter.HEX];
                    continue;
                }
                if(data.matches("[\\dA-Fa-f]{2}")) {
                    codeArray += data;
                    continue;
                }
                if(data.matches("[-+]?\\d+\\.\\d+")) {
                    if(data.charAt(0) == '+') {
                        data = data.substring(1);
                    }
                    codeArray += Converter.convert(data, Converter.FLOAT)[Converter.HEX];
                    continue;
                }
                if(data.matches("[01]{8}[Bb]?")) {
                    if(data.endsWith("b")) {
                        data = data.substring(0, data.length() - 1);
                    }
                    codeArray += Converter.convert(data, Converter.BIN)[Converter.HEX];
                    continue;
                }
                try {
                    if(data.charAt(0) == '+') {
                        data = data.substring(1);
                    }
                    int integer = Integer.parseInt(data);
                    if(integer < -128 || integer > 127) {
                        throw new RuntimeException("Value " + data + " is out of range!!!");
                    }
                    codeArray += Converter.convert(integer, Converter.DEC)[Converter.HEX];
                }
                catch(NumberFormatException e) {
                    throw new RuntimeException(e.getMessage());
                }
            }
            if(row % 2 != 0) {
                row--;
            }
            return codeArray;
        }
        return semanticMessage(line);
    }
    
    private String semanticMessage(String line) {
        line = line.toUpperCase(new Locale("en", "GB"));
        if(line.startsWith("MOV")) {
            return "\nBM:Instruction Format:\nMOV value -> Rn\nMOV Rm -> Rn\nMOV [xy] -> Rn\n"
                    + "MOV Rn -> [xy]\nMOV [Rm] -> Rn\nMOV Rn -> [Rm]\n\nPress the Assembler Help button for more details";
        }
        else if(line.startsWith("ROT")) {
            return "\nBM:Instruction Format:\nROT Rn, x\n\nPress the Assembler Help button for more details";
        }
        else if(line.startsWith("ADDI")) {
            return "\nBM:Instruction Format:\nADDI Rn, Rm -> Rp\n\nPress the Assembler Help button for more details";
        }
        else if(line.startsWith("ADDF")) {
            return "\nBM:Instruction Format:\nADDF Rn, Rm -> Rp\n\nPress the Assembler Help button for more details";
        }
        else if(line.startsWith("OR")) {
            return "\nBM:Instruction Format:\nOR Rn, Rm -> Rp\n\nPress the Assembler Help button for more details";
        }
        else if(line.startsWith("AND")) {
            return "\nBM:Instruction Format:\nAND Rn, Rm -> Rp\n\nPress the Assembler Help button for more details";
        }
        else if(line.startsWith("XOR")) {
            return "\nBM:Instruction Format:\nXOR Rn, Rm -> Rp\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("JMPEQ.*")) {
            return "\nBM:Instruction Format:\nJMPEQ xy, Rm\nJMPEQ Rn, Rm\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("JMPNE.*")) {
            return "\nBM:Instruction Format:\nJMPNE Rn, Rm\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("JMPGE.*")) {
            return "\nBM:Instruction Format:\nJMPGE Rn, Rm\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("JMPLE.*")) {
            return "\nBM:Instruction Format:\nJMPLE Rn, Rm\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("JMPGT.*")) {
            return "\nBM:Instruction Format:\nJMPGT Rn, Rm\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("JMPLT.*")) {
            return "\nBM:Instruction Format:\nJMPLT Rn, Rm\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("NOP.*")) {
            return "\nBM:Instruction Format:\nNOP and nothing else here\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("HALT.*")) {
            return "\nBM:Instruction Format:\nHALT and nothing else here\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("DATA.*")) {
            return "\nBM:Instruction Format:\nDATA values\nDATA string\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("JMP.*")) {
            return "\nBM:Instruction Format:\nJMP xy\nJMP Rn\n\nPress the Assembler Help button for more details";
        }
        else if(line.matches("\\w+:.*")) {
            return "\nBM:Labels must be at least 4 characters long, starting with a letter, then any of these [A-Za-z0-9_]";
        }
        return "";
    }
    
    private String getToBeReplaced(ArrayList<String> list) {
        for(String line: list) {
            if(line.startsWith("!!")) {
                return line.substring(2, line.length());
            }
        }
        return null;
    }
    
    private void setToBeReplaced(String replacement, ArrayList<String> list) {
        int index = 0;
        for(String line: list) {
            if(line.startsWith("!!")) {
                break;
            }
            index++;
        }
        list.set(index, replacement);
    }
}