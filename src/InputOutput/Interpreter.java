package InputOutput;

/**
 * This class converts the hexadecimal values obtained from 
 * the memory table cells to either assembly code or human friendly instructions
 * which are then printed to the screen. There is only one public static method in this class.
 * 
 * @author Milan Gritta
 */
public class Interpreter {
    
    private Interpreter() {  }

    /**
     * Get instruction String. The machine code is converted to a String text which is
     * then printed in the instructions column in the memory table.
     * @param type the type of instruction, first character of a four digit code
     * @param arg the other three characters of a four digit code
     * @param isAssembler indicated whether to return assembly code or long description String
     * @return instruction as string
     */
    public static String getInstruction(char type, String arg, boolean isAssembler) {
        String response;
        switch(type) {
            case '0': response = no0(arg, isAssembler);
                break;
            case '1': response = no1(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '2': response = no2(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '3': response = no3(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '4': response = no4(arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '5': response = no5(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '6': response = no6(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '7': response = no7(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '8': response = no8(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case '9': response = no9(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case 'A': response = noA(arg.charAt(0),arg.charAt(2), isAssembler);
                break;
            case 'B': response = noB(arg.charAt(0),arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case 'C': response = noC(isAssembler, arg);
                break;
            case 'D': response = noD(arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case 'E': response = noE(arg.charAt(1),arg.charAt(2), isAssembler);
                break;
            case 'F': response = noF(arg.charAt(0),arg.charAt(1), arg.charAt(2), isAssembler);
                break;
            default: return "";
        }
        return response;
    }
    
    private static String no0(String arg, boolean isAssembler) {
        if(!arg.equals("FFF")) {
            return "";
        }
        return (isAssembler) ? "NOP" : "No operation";
    }
    
    private static String no1(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "MOV [" + arg2 + arg3 + "] -> R" + arg1 
               : "Load reg " + arg1 + " from memory cell " + arg2 + arg3;
    }
    
    private static String no2(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "MOV " + arg2 + arg3 + " -> R" + arg1
               : "Load register " + arg1 + " with value " + arg2 + arg3;
    }
    
    private static String no3(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "MOV R" + arg1 + " -> [" + arg2 + arg3 + "]" 
               : "Store register " + arg1 + " in memory cell " + arg2 + arg3;
    }
    
    private static String no4(char arg1, char arg2, boolean isAssembler) {
        return (isAssembler) ? "MOV R" + arg1 + " -> R" + arg2 
               : "Copy register " + arg1 + " to register " + arg2;
    }
    
    private static String no5(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "ADDI R" + arg2 + ", R" + arg3 + " -> R" + arg1 
               : "Put reg " + arg2 + " + reg " + arg3 + " (ints) in reg " + arg1;
    }
    
    private static String no6(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "ADDF R" + arg2 + ", R" + arg3 + " -> R" + arg1
               : "Put reg " + arg2 + " + reg " + arg3 + " (floats) in reg " + arg1;
    }
    
    private static String no7(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "OR R" + arg2 + ", R" + arg3 + " -> R" + arg1
               : "Put reg " + arg2 + " OR reg " + arg3 + " in reg " + arg1;
    }
    
    private static String no8(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "AND R" + arg2 + ", R" + arg3 + " -> R" + arg1
               : "Put reg " + arg2 + " AND reg " + arg3 + " in reg " + arg1;
    }
    
    private static String no9(char arg1, char arg2, char arg3, boolean isAssembler) {
        return (isAssembler) ? "XOR R" + arg2 + ", R" + arg3 + " -> R" + arg1
               : "Put reg " + arg2 + " XOR reg " + arg3 + " in reg " + arg1;
    }
    
    private static String noA(char arg1, char arg2, boolean isAssembler) {
        return (isAssembler) ? "ROT R" + arg1 + ", " + arg2
               : "Rotate register " + arg1 + " by " + arg2 + " bits right";
    }
    
    private static String noB(char arg1, char arg2, char arg3, boolean isAssembler) {
        if(arg1 == '0') {
            return (isAssembler) ? "JMP " + arg2 + arg3 : "Jump to " + arg2 + arg3;
        }
        else {
            return (isAssembler) ? "JMPEQ " + arg2 + arg3 + ", R" + arg1
                    : "Jump to " + arg2 + arg3 + " if reg " + arg1 + " equals reg 0";
        }
    }
    
    private static String noC(boolean isAssembler, String args) {
        return args.equals("000") ? (isAssembler) ? "HALT" : "Halt" : "";
    }
    
    private static String noD(char arg1, char arg2, boolean isAssembler) {
        return (isAssembler) ? "MOV [R" + arg2 + "] -> R" + arg1
               : "Load reg " + arg1 + " from memory addr in R" + arg2;
    }
    
    private static String noE(char arg1, char arg2, boolean isAssembler) {
        return (isAssembler) ? "MOV R" + arg1 + " -> [R" + arg2 + "]"
               : "Store R" + arg1 + " in memory addr in R" + arg2;
    }
    
    private static String noF(char arg1, char type, char arg2, boolean isAssembler) {
        String operation = "JMP";
        switch(type) {
            case '0':operation = (isAssembler) ? operation + "EQ" : "equals R0";
                break;
            case '1':operation = (isAssembler) ? operation + "NE" : "not equal R0";
                break;
            case '2':operation = (isAssembler) ? operation + "GE" : "gt or eq. R0";
                break;
            case '3':operation = (isAssembler) ? operation + "LE" : "less or eq. R0";
                break;
            case '4':operation = (isAssembler) ? operation + "GT" : "greater than R0";
                break;
            case '5':operation = (isAssembler) ? operation + "LT" : "less than R0";
                break;
            default: return "";
        }
        return (isAssembler) ? operation + " R" + arg2 + ", R" + arg1
               : "Jump to R" + arg2 + " if R" + arg1 + " " + operation;
    }
}
