package MainEngine;

import GUI.MainWindow;
import java.awt.HeadlessException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * The main class containing only the main method to set this application
 * into motion.
 * 
 * @author Milan Gritta
 */
public class BM {

    /**
     * Main method implementation.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //EXCLUDE FROM PROJECT - **/*.java,**/*.form
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            new MainWindow().setVisible(true);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            
        }
    }
    
    /**
     * Logs any serious unforeseen problems to be reported to developer.
     * @param e exception with the details of failure
     */
    @SuppressWarnings({"null", "ConstantConditions"})
    public static void log(Exception e) {
        BufferedWriter output = null;
        try {
            MainWindow.printText("Something went wrong!!!\nA BUG REPORT has been created in this program's directory."
                    + "\nPlease forward it to your course leader. Thank you");
            File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + "BUG_REPORT.txt");
            output = new BufferedWriter(new FileWriter(file));
            output.write("Localised Message:" + e.getLocalizedMessage());
            output.write("\nMessage:" + e.getMessage());
            output.write("\n" + e.toString() + "\n");
            for(StackTraceElement s : e.getStackTrace()) {
                output.write(s.toString() + "\n");
            }
            output.write("Dated:" + new Date().toString());
            JOptionPane.showMessageDialog(null, "Please include any file you were trying to load. Thank you");
        } catch (IOException | HeadlessException ex) {
            JOptionPane.showMessageDialog(null, "Sorry, could not create report :-(\nPlease report it...");
        } finally {
            try {
                output.close();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "If you see this, then today really isn't your day :-(");
            }
        }
    }
}