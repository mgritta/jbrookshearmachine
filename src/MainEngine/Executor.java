package MainEngine;

import GUI.MainWindow;
import InputOutput.Converter;
import InputOutput.Interpreter;
import java.util.Stack;
import javax.swing.JTable;

/**
 * The main class for executing instructions. It is the main engine behind all execution.
 * It handles all error checking, looping, speed of execution and triggering of memory and register cells
 * updates and bitmap display updates.
 * 
 * @author Milan Gritta
 */
public class Executor implements Runnable {
    private JTable mem, reg;
    private static int sleepingTime = 500, clockCycles = 0;
    private Implementer exec;
    private static Stack<StackItem> stack;
    
    /**
     * Default constructor for class Executor with two
     * arguments.
     * @param m the memory table object
     * @param r the register table object
     */
    public Executor(JTable m, JTable r) {
        exec = new Implementer();
        stack = new Stack<>();
        mem = m;
        reg = r;
    }

    /**
     * The run() of Runnable. This is the loop of main execution
     * which continues until the HALT instruction is received or
     * an invalid instruction is encountered.
     */
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        try {
            int status = execute();
            while(status != 1) {
                try {
                    Thread.sleep(sleepingTime);
                }
                catch(InterruptedException e) {
                    MainWindow.printText("Stopped by user...");
                    MainWindow.setButtonText("Continue");
                    return;
                }
                status = execute();
            }
        }
        catch(Exception e) {
            MainWindow.printText("Invalid Instruction!\n\n" + e.getMessage());
        }
        MainWindow.setButtonText("Continue");
    }
    
    /**
     * Execute one instruction only. This method is called repeatedly in the loop
     * when the Reset&Run button starts continuous execution.
     * 
     * @return status number - error reporting code
     */
    public int execute() {
        int cellNumber;
        int counter = Integer.parseInt(MainWindow.getCounter(), 16);
        String instruction = mem.getValueAt(counter, 2).toString();
        instruction += mem.getValueAt(counter + 1, 2).toString();
        
        if(instruction.equals("C000")) {
            return 1;
        }
        switch(instruction.charAt(0)) {
            case '0': if(!instruction.equals("0FFF")) {
                mem.setRowSelectionInterval(counter, counter);
                mem.setColumnSelectionInterval(0, 6);
                throw new RuntimeException("Programming error in cell " + Converter.decToHex(counter) 
                        + "!\n\nTried to execute a non-executable instruction (BM Opcode: 0)" + 
                        "\n\nTo ignore a cell, use NOP (assembly) or 0FFF (machine) code...");
            }
                break;                
            case '1': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.loadMemoryToRegister(instruction.substring(2, 4), "" + instruction.charAt(1), mem, reg); 
                      updateRegisters(cellNumber);
                break;
            case '2': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.loadValueToRegister(instruction.substring(1,2), instruction.substring(2, 4), reg);
                      updateRegisters(cellNumber);
                break;
            case '3': this.updateStack(instruction.substring(2, 4), true, counter);
                      cellNumber = exec.storeRegisterInMemory("" + instruction.charAt(1), instruction.substring(2, 4), reg, mem);
                      updateMemory(cellNumber);
                break;
            case '4': this.updateStack(instruction.substring(3, 4), false, counter);
                      cellNumber = exec.moveRegToReg(instruction.substring(2, 3), instruction.substring(3, 4), reg);
                      updateRegisters(cellNumber);
                break;
            case '5': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.addInts("" + instruction.charAt(2), "" + instruction.charAt(3), "" + instruction.charAt(1), reg);
                      updateRegisters(cellNumber);
                break;
            case '6': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.addFloats("" + instruction.charAt(2),"" + instruction.charAt(3), "" + instruction.charAt(1), reg);
                      updateRegisters(cellNumber);
                break;
            case '7': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.or(instruction.substring(2, 3), instruction.substring(3, 4), instruction.substring(1, 2), reg);
                      updateRegisters(cellNumber);
                break;
            case '8': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.and(instruction.substring(2, 3), instruction.substring(3, 4), instruction.substring(1, 2), reg);
                      updateRegisters(cellNumber);
                break;
            case '9': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.xor(instruction.substring(2, 3), instruction.substring(3, 4), instruction.substring(1, 2), reg);
                      updateRegisters(cellNumber);
                break;
            case 'A': this.updateStack(instruction.substring(1, 2), false, counter);
                      cellNumber = exec.rotateBits(instruction.substring(1, 2), instruction.substring(3, 4), reg);
                      updateRegisters(cellNumber);
                break;
            case 'B': stack.push(new StackItem(true, null, 0, counter));
                      cellNumber = exec.jumpIfEquals(instruction.substring(1, 2), instruction.substring(2, 4), reg);
                      counter = (cellNumber == -1) ? counter : cellNumber;
                break;
            case 'C': if(!instruction.equals("C000")) {
                        throw new RuntimeException("Programming error in cell " + Converter.decToHex(counter) 
                        + "!\n\nTried to execute a non-executable instruction (BM Opcode: C)" + 
                        "\n\nTo stop execution, use HALT (assembly) or C000 (machine) code...");
                      }
                break;
            case 'D': this.updateStack(instruction.substring(2, 3), false, counter);
                      cellNumber = exec.loadMemoryToR(instruction.substring(3, 4), instruction.substring(2, 3), reg, mem);
                      updateRegisters(cellNumber);
                break;
            case 'E': int cell = Integer.parseInt(reg.getValueAt(Integer.parseInt(instruction.substring(3, 4),16), 2).toString(),16);
                      stack.push(new StackItem(true, mem.getValueAt(cell, 2), cell, counter));
                      cellNumber = exec.storeRegInMem(instruction.substring(2, 3), instruction.substring(3, 4), reg, mem);
                      updateMemory(cellNumber);
                break;
            case 'F': stack.push(new StackItem(true, null, 0, counter));
                      cellNumber = exec.jumpIfCondition(instruction.charAt(2), instruction.substring(1, 2), instruction.substring(3, 4), reg);
                      counter = (cellNumber == -1) ? counter : cellNumber - 2;
                break;
            default: throw new RuntimeException("Invalid instruction!!!"); //This should NEVER happen!!!
        }
        counter += 2;
        MainWindow.printText("Instructions: " + ++clockCycles);
        if(counter == 256) {
            MainWindow.printText("End of memory reached...");
            return 1;
        }
        mem.setRowSelectionInterval(counter, counter);
        mem.setColumnSelectionInterval(0, 6);
        MainWindow.setCounter(Converter.decToHex(counter));
        if(stack.size() > 750) { 
            stack.setSize(500);
        }
        return 0;
    }
    
    /**
     * Set the new sleeping time. The execution pauses for a set amount of time
     * with each iteration of the loop. The length of that pause is set here.
     * 
     * @param sleep time in milliseconds MIN SLEEP:1ms MAX SLEEP:1001ms
     */
    public static void setSleepingTime(int sleep) {
        sleepingTime = 1+ (100 - sleep) * 10;
    }
    
    private void updateRegisters(int row) {
        Object cellContents = reg.getValueAt(row, 2);
        Object [] array = Converter.convert(cellContents, Converter.HEX);
        reg.setValueAt(array[4], row, 5);
        reg.setValueAt(array[3], row, 4);
        reg.setValueAt(array[2], row, 3);
        reg.setValueAt(array[1], row, 2);
        reg.setValueAt(array[0], row, 1);
    }
    
    private void updateMemory(int row) {
        updateCell(row);
        if(row % 2 != 0) {
            updateCell(row - 1);
        }
        if(row > 127 && row < 256) {
            MainWindow.updateBitmap();
        }
    }
    
    private void updateCell(int row) {
        String cellString;
        if(row == 255) {
            cellString = mem.getValueAt(row, 2).toString();
        }
        else {
            cellString = mem.getValueAt(row, 2).toString().concat(mem.getValueAt(row + 1, 2).toString());
        }
        Object [] array = Converter.convert(cellString.substring(0, 2), Converter.HEX);
        mem.setValueAt(array[4], row, 5);
        mem.setValueAt(array[3], row, 4);
        mem.setValueAt(array[2], row, 3);
        mem.setValueAt(array[1], row, 2);
        mem.setValueAt(array[0], row, 1);
        if(row % 2 == 0) {
            mem.setValueAt(Interpreter.getInstruction(cellString.charAt(0), 
                               cellString.substring(1, 4), MainWindow.isAssembler()),row,6);
        }
    }
    
    /**
     * Resets the clockCycles counter to zero.
     */
    public void resetClockCycles() {
        clockCycles = 0;
    }
    
    /**
     * Performs a step back by popping last action off the stack.
     */
    public void doStepBack() {
        if(stack.isEmpty()) {
            MainWindow.printText("Can't go back, stack empty...");
            return;
        }
        MainWindow.printText("Instructions: " + --clockCycles);
        StackItem s = stack.pop();
        if(s.value != null) {
            if(s.isMemory) {
                this.mem.setValueAt(s.value, s.cellNum, 2);
                this.updateMemory(s.cellNum);
            }
            else {
               this.reg.setValueAt(s.value, s.cellNum, 2);
               this.updateRegisters(s.cellNum);
            }   
        }
        mem.setRowSelectionInterval(s.progCounter, s.progCounter);
        mem.setColumnSelectionInterval(0, 6);
        MainWindow.setCounter(Converter.decToHex(s.progCounter));
    }
    
    /**
     * A reset button had been pressed so we clear the contents of the stack
     * ready to start a fresh run.
     */
    public void resetStack() {
        stack.clear();
    }
    
    private void updateStack(String location, boolean isMemory, int counter) {
        int cell = Integer.parseInt(location, 16);
        if(isMemory) {
            stack.push(new StackItem(isMemory, mem.getValueAt(cell, 2), cell, counter));     
        }
        else {
            stack.push(new StackItem(isMemory, reg.getValueAt(cell, 2) , cell, counter));
        }
    }
    
    private class StackItem {
        boolean isMemory;
        Object value;
        int cellNum, progCounter;

        public StackItem(boolean isMemory, Object value, int cellNum, int prog) {
            this.isMemory = isMemory;
            this.value = value;
            this.cellNum = cellNum;
            this.progCounter = prog;
        }
    }
}