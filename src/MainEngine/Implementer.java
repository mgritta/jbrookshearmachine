package MainEngine;

import InputOutput.Converter;
import java.util.ArrayList;
import javax.swing.JTable;

/**
 * This class defines the individual instructions. It implements all
 * 16 operations of the extended BM such as AND, OR, XOR and many more.
 * 
 * @author Milan Gritta
 */
public class Implementer {
    
    /**
     * Empty default constructor. Nothing interesting happening here.
     */
    protected Implementer() {  }
    
    /**
     * Loads the register with the value from a memory address given in the instruction. 
     * @param memoryCell the cell where to look for the value
     * @param register the cell where to load that value
     * @param m the memory table object
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int loadMemoryToRegister(String memoryCell, String register, JTable m, JTable r) {
        int mem = Integer.parseInt(memoryCell, 16);
        int reg = Integer.parseInt(register, 16);
        r.setValueAt(m.getValueAt(mem, 2), reg, 2);
        return reg;
    }
    
    /**
     * Loads a value to a register cell.
     * @param reg the cell to load value into
     * @param value the actual value
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int loadValueToRegister(String reg, String value, JTable r) {
        int register = Integer.parseInt(reg,16);
        r.setValueAt(value, register, 2);
        return register;
    }
    
    /**
     * Store a register value in a memory cell with address specified in the argument.
     * @param reg the cell to take the value from
     * @param mem the cell to save the value to
     * @param r the register table object
     * @param m the memory table object
     * @return cell number to be updated in hexadecimal
     */
    protected int storeRegisterInMemory(String reg, String mem, JTable r, JTable m) {
        int register = Integer.parseInt(reg,16);
        int memory = Integer.parseInt(mem,16);
        m.setValueAt(r.getValueAt(register, 2), memory, 2);
        return memory;
    }
    
    /**
     * Move a register value to another register.
     * @param from the cell to take the value from
     * @param to the cell to save the value to
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int moveRegToReg(String from, String to, JTable r) {
        int fromReg = Integer.parseInt(from,16);
        int toReg = Integer.parseInt(to,16);
        r.setValueAt(r.getValueAt(fromReg, 2), toReg, 2);
        return toReg;
    }
    
    /**
     * Add two registers as integers.
     * @param first the cell to extract the first value from
     * @param second the cell to extract the second value from
     * @param to the cell to save the sum to
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int addInts(String first, String second, String to, JTable r) {
        int regOne = Integer.parseInt(first,16);
        int regTwo = Integer.parseInt(second,16);
        int toReg = Integer.parseInt(to,16);
        int value1 = Integer.parseInt(r.getValueAt(regOne, 4).toString());
        int value2 = Integer.parseInt(r.getValueAt(regTwo, 4).toString());
        int result = value1 + value2;
        result = (result > 127) ? result - 256 : result;
        result = (result < -128) ? result + 256 : result;
        if(result > 127 || result < -128) {
            throw new RuntimeException("Integer " + result + " is out of range (-128 to 127)");
        }
        r.setValueAt(Converter.convert(result, Converter.DEC)[Converter.HEX],toReg, 2);
        return toReg;
    }
    
    /**
     * Adds up two registers as floats.
     * @param first the cell number of the first value
     * @param second the cell number of the second value
     * @param to the cell number to save the sum to
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int addFloats(String first, String second, String to, JTable r) {
        int regOne = Integer.parseInt(first,16);
        int regTwo = Integer.parseInt(second,16);
        int toReg = Integer.parseInt(to,16);
        float value1 = Float.parseFloat(r.getValueAt(regOne, 5).toString());
        float value2 = Float.parseFloat(r.getValueAt(regTwo, 5).toString());
        float result = value1 + value2;
        if(result > 7.5 || result < -7.5) {
            throw new RuntimeException("Float " + result + " is out of range (-7.5 to 7.5)");
        }
        r.setValueAt(Converter.convert(result, Converter.FLOAT)[Converter.HEX], toReg, 2);
        return toReg;
    }
    
    /**
     * OR each bit of two bytes from the register table.
     * @param first the cell number of the first value
     * @param second the cell number of the second value
     * @param to the cell number to save the result to
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int or(String first, String second, String to, JTable r) {
        Object [] values = prepareValues(first, second, to, r);
        String aByte = "";
        for(int i = 0; i < 8; i++) {
            if(((char[]) values[1])[i] == '0' && ((char[]) values[2])[i] == '0') {
                aByte += '0';
            }
            else {
                aByte += '1';
            }
        }
        r.setValueAt(Converter.convert(aByte, Converter.BIN)[Converter.HEX], Integer.parseInt(values[0].toString()), 2);
        return Integer.parseInt(values[0].toString());
    }
    
    /**
     * AND each bit of two bytes from the register table.
     * @param first the cell number of the first value
     * @param second the cell number of the second value
     * @param to the cell number to save the result to
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int and(String first, String second, String to, JTable r) {
        Object [] values = prepareValues(first, second, to, r);
        String aByte = "";
        for(int i = 0; i < 8; i++) {
            if(((char[]) values[1])[i] == '1' && ((char[]) values[2])[i] == '1') {
                aByte += '1';
            }
            else {
                aByte += '0';
            }
        }
        r.setValueAt(Converter.convert(aByte, Converter.BIN)[Converter.HEX], Integer.parseInt(values[0].toString()), 2);
        return Integer.parseInt(values[0].toString());
    }
    
    /**
     * XOR each bit of two bytes from the register table.
     * @param first the cell number of the first value
     * @param second the cell number of the second value
     * @param to the cell number to save the result to
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int xor(String first, String second, String to, JTable r) {
        Object [] values = prepareValues(first, second, to, r);
        String aByte = "";
        for(int i = 0; i < 8; i++) {
            if((((char[]) values[1])[i] == '1' && ((char[]) values[2])[i] == '1') ||
               (((char[]) values[1])[i] == '0' && ((char[]) values[2])[i] == '0')) {
                aByte += '0';
            }
            else {
                aByte += '1';
            }
        }
        r.setValueAt(Converter.convert(aByte, Converter.BIN)[Converter.HEX], Integer.parseInt(values[0].toString()), 2);
        return Integer.parseInt(values[0].toString());
    }
    
    /**
     * Rotate each bit of byte in a register table cell.
     * @param register the cell number of the bit pattern to rotate
     * @param bitsToMove how many bits to rotate by
     * @param r the register table object
     * @return cell number to be updated in hexadecimal
     */
    protected int rotateBits(String register, String bitsToMove, JTable r) {
        int reg = Integer.parseInt(register, 16);
        int noOfTimes = Integer.parseInt(bitsToMove,16);
        char [] bits = (r.getValueAt(reg, 1).toString()).toCharArray();
        ArrayList<Character> list = new ArrayList<>();
        String newByte = "";
        for(int j = 0; j < 8; j++) {
            list.add(bits[j]);
        }
        for(int i = 0; i < noOfTimes; i++) {
            list.add(0, list.get(7));
        }
        for(int e = 0; e < 8; e++) {
            newByte += list.get(e);
        }
        r.setValueAt(Converter.convert(newByte, Converter.BIN)[Converter.HEX], reg, 2);
        return reg;
    }
    
    /**
     * Jumps to new memory location if specified register is equal to register 0
     * or if and only if the second character is 0, jump without testing.
     * @param register the cell number to be compared
     * @param memory the cell number in memory if test successful
     * @param r the register table object
     * @return -1 if test unsuccessful, the memory cell number if successful.
     */
    protected int jumpIfEquals(String register, String memory, JTable r) {
        int reg = Integer.parseInt(register, 16);
        if(reg == 0) {
            return Integer.parseInt(memory,16) - 2;
        }
        if(r.getValueAt(reg, 2).toString().equals(r.getValueAt(0, 2).toString())) {
            return Integer.parseInt(memory,16) - 2;
        }
        return -1;
    }
    
    /**
     * Load register cell with the value in the memory cell held in another register. 
     * @param from the cell number in the register table with the memory address
     * @param to the cell number to load in the register table
     * @param r the register table object
     * @param m the memory table object
     * @return cell number to be updated in hexadecimal.
     */
    protected int loadMemoryToR(String from, String to, JTable r, JTable m) {
        int fromR = Integer.parseInt(from,16);
        int toR = Integer.parseInt(to,16);
        int address = Integer.parseInt(r.getValueAt(fromR, 2).toString(),16);
        Object value = m.getValueAt(address, 2);
        r.setValueAt(value, toR, 2);
        return toR;
    }
    
    /**
     * Store register value in memory cell held in another register.
     * @param from the cell number to extract the value from
     * @param to the cell number of the register where the memory address is held
     * @param r the register table object
     * @param m the memory table object
     * @return cell number to be updated in hexadecimal
     */
    protected int storeRegInMem(String from, String to, JTable r, JTable m) {
        int fromR = Integer.parseInt(from,16);
        int toM = Integer.parseInt(to,16);
        int address = Integer.parseInt(r.getValueAt(toM, 2).toString(),16);
        Object value = r.getValueAt(fromR, 2);
        m.setValueAt(value, address, 2);
        return address;
    }
    
    /**
     * Jump to memory cell if condition is true. Uses multiple conditions
     * which are specified in the first argument.
     * @param cond the test condition to be performed
     * @param register the cell number of the value to be compared
     * @param to the cell number where the memory address is stored 
     * @param r the register table object
     * @return -1 if test unsuccessful and address in memory to jump to if successful
     */
    protected int jumpIfCondition(char cond, String register, String to, JTable r) {
        String reg0 = r.getValueAt(0, 2).toString();
        String compReg = r.getValueAt(Integer.parseInt(register,16), 2).toString();
        int address = Integer.parseInt(r.getValueAt(Integer.parseInt(to,16), 2).toString(),16);
        switch(cond) {
            case '0': if(reg0.equals(compReg)) { return address; }
                break;
            case '1': if(!reg0.equals(compReg)) { return address; }
                break;
            case '2': if(Integer.parseInt(compReg,16) >= Integer.parseInt(reg0,16)) { return address; }
                break;
            case '3': if(Integer.parseInt(compReg,16) <= Integer.parseInt(reg0,16)) { return address; }
                break;
            case '4': if(Integer.parseInt(compReg,16) > Integer.parseInt(reg0,16)) { return address; }
                break;
            case '5': if(Integer.parseInt(compReg,16) < Integer.parseInt(reg0,16)) { return address; }
                break;
            default: throw new RuntimeException("The condition in the JMP instruction is invalid!!");
        }
        return -1;
    }
    
    private Object [] prepareValues(String first, String second, String to, JTable r) {
        int regOne = Integer.parseInt(first,16);
        int regTwo = Integer.parseInt(second,16);
        int toReg = Integer.parseInt(to,16);
        char [] byte1 = r.getValueAt(regOne, 1).toString().toCharArray();
        char [] byte2 = r.getValueAt(regTwo, 1).toString().toCharArray();
        Object [] bits = new Object[3];
        bits[0] = toReg;
        bits[1] = byte1;
        bits[2] = byte2;
        return bits;
    }
}
