<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN">
<html xmlns:mwsh="http://www.mathworks.com/namespace/mcode/v1/syntaxhighlight.dtd">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Extended Brookshear Machine Assembler</title>
<style>
body {
  background-color: white;
  margin:10px;
  font-family:"Microsoft JhengHei", cursive;
}

h1 {
  color:#666699; 
  font-size: x-large;
}

h2 {
  color:#006633;
  font-size: medium;
}

p,h1,h2,div.content div {
  max-width: 600px;
  /* Hack for IE6 */
  width: auto !important; width: 600px;
}

pre.codeinput {
  background: #EEEEEE;
  padding: 10px;
} 

span.keyword {color: #0000FF}
span.comment {color: #228B22}
span.string {color: #A020F0}
span.untermstring {color: #B20000}
span.syscmd {color: #B28C00}

pre.codeoutput {
  color: #666666;
  padding: 10px;
}

pre.error {
  color: red;
}

p.footer {
  text-align: right;
  font-size: xx-small;
  font-weight: lighter;
  font-style: italic;
  color: gray;
}

  </style>
</head>
   <body>
      <div class="content">
         <h1>Extended Brookshear Machine Assembler</h1>
         <introduction>
            <p>This is an assembler for the machine described in <i>Computer Science: An Overview</i>, 10th edition,  by J. Glenn Brookshear (Pearson Education, 2008). The machine is extended with three additional instructions.
            </p>
            <p>For information about the machine architecture, see <a href="bmhelp.html">help information</a> for the emulator.
         </p>
         </introduction>
         <h2>Contents</h2>
         <div>
            <ul>
               <li><a href="#1">Conventions</a></li>
               <li><a href="#2">File format</a></li>
               <li><a href="#3">Assembly and loading process</a></li>
               <li><a href="#4">MOV instruction</a></li>
               <li><a href="#5">Register operation instructions</a></li>
               <li><a href="#6">Control instructions</a></li>
               <li><a href="#7">DATA instruction</a></li>
               <li><a href="#8">Addresses and Labels</a></li>
               <li><a href="#9">Example</a></li>
            </ul>
         </div>
         <h2>Conventions<a name="1"></a></h2>
         <p>Text in <i>italics</i> in a format is a placeholder: it needs to be replaced with an appropriate character sequence to make a legal instruction.
            The characters m, n, p, x and y stand for hex characters.
         </p>
         <p>Hex characters are 0-9, A-F and a-f.</p>
         <h2>File format<a name="2"></a></h2>
         <p>The assembly language file is a plain text file and can be prepared using a text editor such as the Matlab editor, WordPad,
            Notepad, Emacs or vi. It is read as a sequence of lines. Each line contains one statement, with the format
         </p>
         <p><i>location</i>  <i>instruction</i>  <i>comment</i></p>
         <p>where</p>
         <div>
            <ul>
               <li><i>location</i> has the format xy: or <i>label</i>: (note the colon at the end) and is described below in the section "Addresses and Labels".
               </li>
               <li><i>instruction</i> has the format <i>OP args</i>, where <i>OP</i> specifies an operation. Instructions are described in the sections below.
               </li>
               <li><i>comment</i> has the format // <i>text</i> and is ignored.
               </li>
            </ul>
         </div>
         <p>Any or all of the three components may be omitted. White space before any of the components is ignored.</p>
         <p>The first example below has all three components, the second has only a location and a comment, and the third has only an
            instruction:
         </p><pre> loop:   ADDI R1, R2 -&gt; R3   // increment loop counter
 A0:     // next instruction will be loaded at location A0
 ROT  R1, 4</pre><h2>Assembly and loading process<a name="3"></a></h2>
         <p>Each assembly language instruction, except DATA, is translated into a single Brookshear Machine (BM) instruction. The code
            for this instruction is allocated to two memory cells, starting at the next free cell unless this is overriden by an explicit
            address in the <i>location</i> field (see "Addresses and Labels" below).
         </p>
         <p>BM execution normally starts from address 0, and so the first instruction in the program should usually be stored at address
            0. This is the default if no explicit address is given.
         </p>
         <p>The output of the assembler can be loaded directly into memory or saved as a machine code file, which may then be loaded.
            These options are described in the <a href="bmhelp.html">general help for the emulator</a>.
         </p>
         <h2>MOV instruction<a name="4"></a></h2>
         <p>The MOV instruction moves (or more precisely copies) a byte of data from one location to another. Its general format is</p>
         <p>MOV <i>source</i> -&gt; <i>destination</i></p>
         <p>The source and destination may be specified in a variety of ways, corresponding to different addressing modes. Six different
            combinations are legal, as follows.
         </p>
         <p><b>MOV</b> <i>value</i> -&gt; Rn
         </p>
         <p>The value is fixed when the assembly language is written, and it is stored in memory as part of the program. (This is immediate-mode
            addressing.) It may be specified in the assembler instruction in  one of the following ways:
         </p>
         <div>
            <ul>
               <li>Two hex characters, optionally followed by "h", e.g. 1Ch, 13, 02. Note that a value of 13 means hexadecimal 13, i.e. decimal
                  19, not decimal 13.
               </li>
               <li>Eight binary characters (0 or 1), optionally followed by "b", e.g. 00010101, 11011111b.</li>
               <li>A signed decimal integer in the range -128 to +127. This is is written with a leading + or - sign followed by 1, 2 or 3 decimal
                  characters (0-9). If it is positive and has only one digit, the sign may be omitted. E.g. -100, 7, +33. The value stored represents
                  the integer in twos complement format.
               </li>
               <li>A floating point number in the range -7.5 to 7.5. This is written with an optional sign, and must contain a decimal point.
                  There must be at least one decimal digit before the point. E.g. 0.0, -3.2, +4., 0.03. The value stored represents the number,
                  or an approximation to it, in the floating-point format described in the emulator documentation and in <i>Computer Science: An Overview</i>.
               </li>
               <li>A single ASCII character, written between quotes. E.g. "c", ",", """, "8". The ASCII code for the character is stored.</li>
               <li>A label. See the section below on addresses and labels.</li>
            </ul>
         </div>
         <p>The destination must be a register. This is written as Rn where n is a hex character which specifies which register receives
            the data.
         </p>
         <p>(BM opcode: 2.)</p>
         <p><b>MOV</b> Rm -&gt; Rn
         </p>
         <p>The value held in register Rm is copied to register Rn, where m and n specify the source and destination registers respectively.
            (BM opcode: 4.)
         </p>
         <p><b>MOV</b> [xy] -&gt; Rn
         </p>
         <p>The value held in the memory cell with address xy is loaded into register Rn. A label may be used instead of an explicit address.
            (The source is specified using direct addressing.) (BM opcode: 1.)
         </p>
         <p><b>MOV</b> Rn -&gt; [xy]
         </p>
         <p>The value held in register Rn is stored in the memory cell with address xy. A label may be used instead of an explicit address.
            (The destination uses direct addressing.) (BM opcode: 3.)
         </p>
         <p><b>MOV</b> [Rm] -&gt; Rn
         </p>
         <p>The value held in the memory cell whose address is in register Rm is loaded into register Rn. (The source uses register indirect
            addressing.) (BM opcode: D.)
         </p>
         <p><b>MOV</b> Rn -&gt; [Rm]
         </p>
         <p>The value held in register Rn is stored in the memory cell whose address is in register Rm. (The destination uses register
            indirect addressing.) (BM opcode: E.)
         </p>
         <h2>Register operation instructions<a name="5"></a></h2>
         <p><b>ROT</b> Rn, x
         </p>
         <p>The bit pattern in register Rn is rotated x bits to the right. For example if x is 1, the rightmost bit is moved to the left
            and every other bit is moved 1 place to the right: 00010001 becomes 10001000 and 01011011 becomes 10101101. Higher values
            of x are equivalent to repeating this process x times altogether. Rn is updated to contain the new pattern. (BM opcode: A.)
         </p>
         <p>Each of the remaining register instructions represents an operation with two inputs and one output. These have the general
            form
         </p>
         <p><i>OP</i> Rn, Rm -&gt; Rp
         </p>
         <p><i>OP</i> specifies the operation to be carried out. Registers Rn and Rm contain the source data, and register Rp is the destination
            in which the result of the operation is stored. Any pair, or all three, of m, n and p may be the same.
         </p>
         <p><b>ADDI</b> Rn, Rm -&gt; Rp
         </p>
         <p>The contents of Rn and Rm are added, assuming that they represent signed integers in twos complement format. (BM opcode: 5.)</p>
         <p><b>ADDF</b> Rn, Rm -&gt; Rp
         </p>
         <p>The contents of Rn and Rm are added, assuming that they represent floating point numbers in the format described in the emulator
            help document and in <i>Computer Science: An Overview</i>. (BM opcode: 6.)
         </p>
         <p><b>OR</b> Rn, Rm -&gt; Rp
         </p>
         <p>A bitwise OR operation is carried out. That is, each bit of Rp is 1 if either of the corresponding bits in Rn and Rm is 1.
            (BM opcode: 7.)
         </p>
         <p><b>AND</b> Rn, Rm -&gt; Rp
         </p>
         <p>A bitwise AND operation is carried out. (BM opcode: 8.)</p>
         <p><b>XOR</b> Rn, Rm -&gt; Rp
         </p>
         <p>A bitwise XOR operation is carried out. (BM opcode: 9.)</p>
         <h2>Control instructions<a name="6"></a></h2>
         <p>Jump instructions cause the program counter to be set to the specified address, so that the next instruction executed is the
            one at that address.
         </p>
         <p><b>JMP</b> xy
         </p>
         <p>Jump to address xy (2 hex digits). The address xy can also be a label - see the section on addresses and labels. (BM opcode:
            B.)
         </p>
         <p><b>JMP</b> Rn
         </p>
         <p>Jump to the address that is held in register Rn. (BM opcode: F.)</p>
         <p><b>JMPEQ</b> xy, Rm
         </p>
         <p>Jump to address xy, if the contents of register Rm are equal to the contents of register R0; otherwise, continue to the next
            instruction in sequence. The address xy can be a label. (BM opcode: B.)
         </p>
         <p><b>JMPEQ</b> Rn, Rm
         </p>
         <p>Jump to the address held in register Rn, if the contents of register Rm are equal to the contents of register R0; otherwise,
            continue to the next instruction in sequence. (BM opcode: F.)
         </p>
         <p>The remaining jump instructions all have the same form as this and are all implemented with BM opcode F.</p>
         <p><b>JMPNE</b> Rn, Rm
         </p>
         <p>Jump to the address in Rn if the contents of Rm are not equal to the contents of R0.</p>
         <p><b>JMPGE</b> Rn, Rm
         </p>
         <p>Jump if the contents of Rm are greater than or equal to the contents of R0, comparing them as unsigned integers.</p>
         <p><b>JMPLE</b> Rn, Rm
         </p>
         <p>Jump if the contents of Rm are less than or equal to the contents of R0, comparing them as unsigned integers.</p>
         <p><b>JMPGT</b> Rn, Rm
         </p>
         <p>Jump if the contents of Rm are greater than the contents of R0, comparing them as unsigned integers.</p>
         <p><b>JMPLT</b> Rn, Rm
         </p>
         <p>Jump if the contents of Rm are less than the contents of R0, comparing them as unsigned integers.</p>
         <p><b>NOP</b></p>
         <p>No operation. This instruction occupies two memory cells, but no actions take place when it is executed. (BM opcode: 0.)</p>
         <p><b>HALT</b></p>
         <p>Halt the machine. (BM opcode: C.)</p>
         <h2>DATA instruction<a name="7"></a></h2>
         <p>The DATA instruction does not translate into a BM instruction, but instead causes data to be loaded into memory alongside
            the program.
         </p>
         <p>If no explicit address is given in a location field, the data are loaded starting at the next free memory location - i.e.
            at the location that would otherwise be used for the next BM instruction. Since the BM starts execution at address 0, this
            means that in most cases DATA statements should come <i>after</i> the program.
         </p>
         <p>The instruction has two forms.</p>
         <p><b>DATA</b> <i>values</i></p>
         <p>Here <i>values</i> is a list separated by commas and optional white space. Each element of the list is data for one byte of memory, and may
            be specified in any of the ways listed for the first form of the MOV instruction. For example
         </p><pre> DATA -123, "s", DE</pre><p>fills three bytes of memory with bit patterns representing the decimal integer -123, the ASCII character s, and the hex number
            DE.
         </p>
         <p><b>DATA</b> <i>string</i></p>
         <p>Here <i>string</i> is a sequence of ASCII characters surrounded by double quotes. The character codes are stored in memory and then "null-terminated"
            - that is, the byte after the last character is set to 00h. For example
         </p><pre> DATA "Some text"</pre><p>fills 10 bytes of memory, 9 with the character codes for <tt>Some text</tt> and one with zero.
         </p>
         <p>The double quote character may be included in the string. The following example could therefore be ambiguous:</p><pre> DATA "a","b"</pre><p>In fact, it is interpreted as a single string - the five character codes for the characters <tt>a " , " b</tt> are put into memory, followed by a zero. To store the character codes for a and b, the following should be used:
         </p><pre> DATA "ab"</pre><h2>Addresses and Labels<a name="8"></a></h2>
         <p>The location field can be used in two ways. First, an explicit address may be given as two hex characters, for example</p><pre> 80: DATA "Text string"</pre><p>This will cause the data to loaded starting from the memory cell with address 80 (hex).</p>
         <p>An address may be specified for any instruction, but its normal use is with a DATA statement, to store data in a specific
            location.
         </p>
         <p>Instructions or DATA statements that do not have addresses follow the previous instruction or data in memory. Thus an explicit
            address affects the position of subsequent instructions. For example
         </p><pre> 1C: DATA 1, 2
     MOV R3 -&gt; R4</pre><p>causes the MOV statement to be stored at address 1E.</p>
         <p>An attempt to re-use a memory location will cause an error at assembly time.</p>
         <p>If an address is given in a statement that does not contain an instruction, the address will be used for the next instruction
            (or data).
         </p>
         <p>The second way to use the location field is for a <b>label</b>. A label is a string of 4 or more characters starting with a letter. The other characters may be letters, digits or underscores.
         </p>
         <p>A label does not affect where anything is stored, but instead records the current address. For example</p><pre> loop: ADDI R1, R2 -&gt; R3</pre><p>causes the address of the ADDI instruction to be associated with the label <tt>loop</tt>.
         </p>
         <p>A label may be used instead of a memory address or value in MOV, JMP and JMPEQ instructions - that is, wherever xy appears
            in an instruction format above. Thus the address associated with a label may be moved to a register, used as the source of
            a load or the destination of a store, or as the target of a jump. For example
         </p><pre> loop: ADDI R1, R2 -&gt; R1
       MOV R1 -&gt; [R4]
       JMP loop</pre><p>causes the ADDI and MOV instructions to be repeated indefinitely.</p>
         <p>A reference to a label (its use as an argument to an instruction) may come before or after the label itself. A label may be
            defined (that is, appear in a location field) only once, but there may be any number of references to it.
         </p>
         <h2>Example<a name="9"></a></h2>
         <p>A program to draw a chessboard pattern in the bitmapped display.</p><pre> // Generates chessboard pattern in the bitmap display</pre><pre> // R1 contains the address at which the next byte of data
 // is to be stored, and is also the loop counter. It is
 // incremented at the start of the loop so is initialised
 // to the location just before the start of display memory.</pre><pre> // As there are 4 bytes per row of the display, and 4 rows of
 // display per row of the chessboard pattern, there are 16
 // bytes per chessboard row. This means that bit 4 of R1
 // (bits numbered 76543210) indicates whether an even or odd row
 // of the chessboard is being generated.</pre><pre> // R3 and R4 contain the two patterns to store in the display,
 // depending on whether an odd or even row is being drawn.</pre><pre>             MOV     [dispmem] -&gt; R1
             MOV     1 -&gt; R2         // constant 1
             MOV     [bwpatt] -&gt; R3
             MOV     [wbpatt] -&gt; R4</pre><pre> startloop:  ADDI    R1, R2 -&gt; R1    // increment loop counter
             MOV     R1 -&gt; RA        // copy it
             ROT     RA, 4           // shift bit 4 to end
             AND     RA, R2 -&gt; RA    // and mask it out
             MOV     1 -&gt; R0         // compare it with 1
             JMPEQ   oddrow, RA      // jump if on an odd row
             MOV     R3 -&gt; [R1]      // store even row pattern
             JMP     endloop
 oddrow:     MOV     R4 -&gt; [R1]      // store odd row pattern</pre><pre> endloop:    MOV     [endmem] -&gt; R0  // last address to fill
             JMPEQ   end_, R1        // reached it?
             JMP     startloop       // no, so loop</pre><pre> end_:       HALT</pre><pre> dispmem:    DATA    7F          // initial address
 endmem:     DATA    FF          // end of memory
 bwpatt:     DATA    00001111    // display pattern 1
 wbpatt:     DATA    11110000    // display pattern 2</pre><p class="footer"><br>
            Published with JAVA&reg; 1.7<br></p>
      </div>
   </body>
</html>
