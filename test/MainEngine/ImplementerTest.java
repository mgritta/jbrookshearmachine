package MainEngine;

import javax.swing.JTable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author milan
 */
public class ImplementerTest {
    
    /**
     *
     */
    public ImplementerTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of loadMemoryToRegister method, of class Implementer.
     */
    @Test
    public void testLoadMemoryToRegister() {
        System.out.println("loadMemoryToRegister");
        String memoryCell = "";
        String register = "";
        JTable m = null;
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.loadMemoryToRegister(memoryCell, register, m, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of loadValueToRegister method, of class Implementer.
     */
    @Test
    public void testLoadValueToRegister() {
        System.out.println("loadValueToRegister");
        String reg = "";
        String value = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.loadValueToRegister(reg, value, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of storeRegisterInMemory method, of class Implementer.
     */
    @Test
    public void testStoreRegisterInMemory() {
        System.out.println("storeRegisterInMemory");
        String reg = "";
        String mem = "";
        JTable r = null;
        JTable m = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.storeRegisterInMemory(reg, mem, r, m);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of moveRegToReg method, of class Implementer.
     */
    @Test
    public void testMoveRegToReg() {
        System.out.println("moveRegToReg");
        String from = "";
        String to = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.moveRegToReg(from, to, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addInts method, of class Implementer.
     */
    @Test
    public void testAddInts() {
        System.out.println("addInts");
        String first = "";
        String second = "";
        String to = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.addInts(first, second, to, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addFloats method, of class Implementer.
     */
    @Test
    public void testAddFloats() {
        System.out.println("addFloats");
        String first = "";
        String second = "";
        String to = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.addFloats(first, second, to, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of or method, of class Implementer.
     */
    @Test
    public void testOr() {
        System.out.println("or");
        String first = "";
        String second = "";
        String to = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.or(first, second, to, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of and method, of class Implementer.
     */
    @Test
    public void testAnd() {
        System.out.println("and");
        String first = "";
        String second = "";
        String to = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.and(first, second, to, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of xor method, of class Implementer.
     */
    @Test
    public void testXor() {
        System.out.println("xor");
        String first = "";
        String second = "";
        String to = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.xor(first, second, to, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of rotateBits method, of class Implementer.
     */
    @Test
    public void testRotateBits() {
        System.out.println("rotateBits");
        String register = "";
        String bitsToMove = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.rotateBits(register, bitsToMove, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of jumpIfEquals method, of class Implementer.
     */
    @Test
    public void testJumpIfEquals() {
        System.out.println("jumpIfEquals");
        String register = "";
        String memory = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.jumpIfEquals(register, memory, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of loadMemoryToR method, of class Implementer.
     */
    @Test
    public void testLoadMemoryToR() {
        System.out.println("loadMemoryToR");
        String from = "";
        String to = "";
        JTable r = null;
        JTable m = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.loadMemoryToR(from, to, r, m);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of storeRegInMem method, of class Implementer.
     */
    @Test
    public void testStoreRegInMem() {
        System.out.println("storeRegInMem");
        String from = "";
        String to = "";
        JTable r = null;
        JTable m = null;
        Implementer instance = new Implementer();
        int expResult = 0;
        int result = instance.storeRegInMem(from, to, r, m);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of jumpIfCondition method, of class Implementer.
     */
    @Test
    public void testJumpIfCondition() {
        System.out.println("jumpIfCondition");
        char cond = ' ';
        String register = "";
        String to = "";
        JTable r = null;
        Implementer instance = new Implementer();
        int expResult = -1;
        int result = instance.jumpIfCondition(cond, register, to, r);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}